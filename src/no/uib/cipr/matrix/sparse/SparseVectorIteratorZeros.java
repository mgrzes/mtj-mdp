package no.uib.cipr.matrix.sparse;

/**
 * Created with IntelliJ IDEA.
 * User: Marek Grzes
 * Date: 03/06/14 11:10 AM
 */

import java.util.Iterator;

/**
 * Iterates over <b>zero elements</b> of the sparse vector.
 * Vector cannot change externally. hasNext() has to be executed before every next().
 */
public class SparseVectorIteratorZeros implements Iterator<Integer> {

	protected SparseVector _vector;
	protected int _position;
	protected int _sparsePosition;
	protected double _tolerance = 0;

	public SparseVectorIteratorZeros(SparseVector vector) {
		_vector = vector;
		_position = 0;
		if ( vector.used > 0 ) {
			_sparsePosition = 0;
		} else {
			_sparsePosition = vector.used;
		}
	}

	public SparseVectorIteratorZeros(SparseVector vector, double tolerance) {
		_tolerance = tolerance;
		_vector = vector;
		_position = 0;
		if ( vector.used > 0 ) {
			_sparsePosition = 0;
		} else {
			_sparsePosition = vector.used;
		}
	}

	/**
	 * Check if the current _position should be moved forward, or if it can stay as it is.
	 */
	protected void shift() {
		// all non-zeros were checked
		if ( _sparsePosition >= _vector.used ) return;

		// the position is before the next non-zero
		if ( _position < _vector.getIndex()[_sparsePosition] ) return;

		if ( _position == _vector.getIndex()[_sparsePosition] ) {
			if ( Math.abs(_vector.getData()[_sparsePosition]) > _tolerance ) {
				_position++;
				_sparsePosition++;
				shift();
			} else {
				_sparsePosition++;
				return;
			}
		}
	}

	public boolean hasNext() {
		// skip all non-zero entries
		shift();

		return _position < _vector.size();
	}

	public Integer next() {
		if ( _position >= _vector.size() ) return null;

		int current = _position;
		_position++;
		shift();

		return current;
	}

	public void remove() {
		// TODO: implement removing from the vector
	}

	/**
	 * reset iterator to the beginning; the vector may be changed externally before this method is called;
	 */
	public void reset() {
		_position = 0;
		if ( _vector.used > 0 ) {
			_sparsePosition = 0;
		} else {
			_sparsePosition = _vector.used;
		}

	}

	public void reset(SparseVector vector) {
		_vector = vector;
		_position = 0;
		if ( vector.used > 0 ) {
			_sparsePosition = 0;
		} else {
			_sparsePosition = vector.used;
		}
	}

	public static void main(String[] args) {
		SparseVector v = new SparseVector(10);
		v.set(5,0.0005);
		v.set(8,0.8);
		SparseVectorIteratorZeros iter = new SparseVectorIteratorZeros(v);
		while (iter.hasNext()) {
			int s = iter.next();
			System.out.print(s + " ");
		}
		System.out.println();
		iter.reset();
		while (iter.hasNext()) {
			int s = iter.next();
			System.out.print(s + " ");
		}
		v.set(0,0.1);
		v.set(9,0.9);
		System.out.println();
		iter.reset(v);
		while (iter.hasNext()) {
			int s = iter.next();
			System.out.print(s + " ");
		}
		System.out.println();
		// everything smaller than 0.001 is rounded to zero
		iter = new SparseVectorIteratorZeros(v,0.001);
		while (iter.hasNext()) {
			int s = iter.next();
			System.out.print(s + " ");
		}
		System.out.println();
	}
}
