package no.uib.cipr.matrix.sparse;

import java.io.Serializable;

/**
 * @author Marek Grzes
 */

/**
 * Belief mask where _index contains indexes from the sparse vector that point to non-zero entries in data of the
 * sparse vector. _used is the number of elements in the mask. <b>_used can be smaller than _index.length</b>
 */
public class SparseVectorMask implements Serializable {
    public SparseVectorMask(int size) {
        _index = new int [size];
    }
    /**
     * When the SparseVector is the belief state, _index contains s of those
     * states where b(s) != 0.
     */
    public int[] _index;
    public int _used;
}
