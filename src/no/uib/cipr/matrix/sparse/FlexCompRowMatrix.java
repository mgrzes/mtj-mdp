/*
 * Copyright (C) 2003-2006 Bjørn-Ove Heimsund
 * 
 * This file is part of MTJ.
 * 
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

package no.uib.cipr.matrix.sparse;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Iterator;

import no.uib.cipr.matrix.*;
import no.uib.cipr.matrix.sparse.SuperIterator.SuperIteratorEntry;

/**
 * Matrix stored row-wise into sparse vectors
 */
public class FlexCompRowMatrix extends AbstractMatrix implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * Matrix data
     */
    SparseVector[] rowD;

    /**
     * Constructor for FlexCompRowMatrix
     * 
     * @param numRows
     *            Number of rows
     * @param numColumns
     *            Number of column
     */
    public FlexCompRowMatrix(int numRows, int numColumns) {
        super(numRows, numColumns);

        rowD = new SparseVector[numRows];
        for (int i = 0; i < numRows; ++i)
            rowD[i] = new SparseVector(numColumns);
    }

	/**
	 * The same as above, but this time SparseVectors that represent rows are not created and are left with null. This
	 * is useful when the matrix reuses some data from another matrix. setRow should be used define all rows of such
	 * a matrix.
	 * @param numRows
	 * @param numColumns
	 * @param initRows
	 */
	public FlexCompRowMatrix(int numRows, int numColumns, boolean initRows) {
		super(numRows, numColumns);

		rowD = new SparseVector[numRows];
		if ( initRows ) {
			for (int i = 0; i < numRows; ++i)
				rowD[i] = new SparseVector(numColumns);
		}
	}

    /**
     * Constructor for FlexCompRowMatrix
     * 
     * @param A
     *            Matrix to copy contents from
     * @param deep
     *            True for a deep copy, false for a reference copy. A reference
     *            copy can only be made of an <code>FlexCompRowMatrix</code>
     */
    public FlexCompRowMatrix(Matrix A, boolean deep) {
        super(A);
        rowD = new SparseVector[numRows];

        if (deep) {
            for (int i = 0; i < numRows; ++i)
                rowD[i] = new SparseVector(numColumns);
            set(A);
        } else {
            FlexCompRowMatrix Ar = (FlexCompRowMatrix) A;
            for (int i = 0; i < numRows; ++i)
                rowD[i] = Ar.getRow(i);
        }
    }

    /**
     * Constructor for FlexCompRowMatrix
     * 
     * @param A
     *            Matrix to copy contents from. The copy will be deep
     */
    public FlexCompRowMatrix(Matrix A) {
        this(A, true);
    }

    /**
     * Returns the given row
     */
    public SparseVector getRow(int i) {
        return rowD[i];
    }

    /**
     * Sets the given row equal the passed vector
     */
    public void setRow(int i, SparseVector x) {
        if (x.size() != numColumns)
            throw new IllegalArgumentException(
                    "New row must be of the same size as existing row");
        rowD[i] = x;
    }

    /**
     * this = this(:,0) * y(0) + ... +  this(:,n) * y(n)
     * Very useful when the TAO matrix is created for POMDPs.
     * @param y
     */
    public void scale(SparseVector y) throws Exception {
        for ( int r = 0; r < numRows; r++ ) {
            int i1 = 0;
            int i2 = 0;
            while ( i1 < rowD[r].used && i2 < y.used ) {
                if ( rowD[r].index[i1] == y.index[i2] ) {
                    rowD[r].data[i1] *= y.data[i2];
                    i1++;
                    i2++;
                    continue;
                }
                if ( rowD[r].index[i1] < y.index[i2] ) {
                    rowD[r].data[i1] = 0.0;
                    i1++;
                    continue;
                }
                if ( rowD[r].index[i1] > y.index[i2] ) {
                    // rowD[r].data[i smaller than i1] is already zero
                    i2++;
                    continue;
                }
            }
            while ( i1 < rowD[r].used ) {
                rowD[r].data[i1++] = 0.0;
            }
            rowD[r].compact();
        }
    }
    
    @Override
    public Vector multAdd(double alpha, Vector x, Vector y) {
        checkMultAdd(x, y);

        for (int i = 0; i < numRows; ++i)
            y.add(i, alpha * rowD[i].dot(x));

        return y;
    }

    protected void checkMultAdd(double[] x, double[] y) {
        if (numColumns != x.length)
            throw new IndexOutOfBoundsException("A.numColumns != x.length ("
                    + numColumns + " != " + x.length + ")");
        if (numRows != y.length)
            throw new IndexOutOfBoundsException("A.numRows != y.length ("
                    + numRows + " != " + y.length + ")");
    }
    
    /**
     * Computes: y = alpha*A*x where A is this matrix.
     * @param alpha
     * @param x
     * @param y
     * @return y
     */
    public double[] mult(double alpha, double[] x, double[] y) {
        if ( MTJRuntimeConfig.MTJDEBUG ) {
            checkMultAdd(x, y);
        }
        
        for (int i = 0; i < numRows; ++i) {
            y[i] = alpha * rowD[i].dot(x);
        }
        
        return y;
    }

    /**
     * Computes: y = A*x where A is this matrix.
     * @param x
     * @param y
     * @return y
     */
    public double[] mult(double[] x, double[] y) {
        if ( MTJRuntimeConfig.MTJDEBUG ) {
            checkMultAdd(x, y);
        }
        
        for (int i = 0; i < numRows; ++i) {
            y[i] = rowD[i].dot(x);
        }
        
        return y;
    }
    
    @Override
    public Vector transMultAdd(double alpha, Vector x, Vector y) {
        if (!(x instanceof DenseVector) || !(y instanceof DenseVector))
            return super.transMultAdd(alpha, x, y);

        checkTransMultAdd(x, y);

        double[] xd = ((DenseVector) x).getData(), yd = ((DenseVector) y)
                .getData();

        // y = 1/alpha * y
        y.scale(1. / alpha);

        // y = A'x + y
        for (int i = 0; i < numRows; ++i) {
            SparseVector v = rowD[i];
            int[] index = v.getIndex();
            double[] data = v.getData();
            int length = v.getUsed();
            for (int j = 0; j < length; ++j)
                yd[index[j]] += data[j] * xd[i];
        }

        // y = alpha*y = alpha * A'x + y
        return y.scale(alpha);
    }

    @Override
    public void add(int row, int column, double value) {
        rowD[row].add(column, value);
    }

    @Override
    public void set(int row, int column, double value) {
        rowD[row].set(column, value);
    }

    @Override
    public double get(int row, int column) {
        return rowD[row].get(column);
    }

    @Override
    public Iterator<MatrixEntry> iterator() {
        return new RowMatrixIterator();
    }

    @Override
    public Matrix copy() {
        return new FlexCompRowMatrix(this);
    }

	/** creates a new col matrix using data from the row matrix; always deep; fast implementation possible thanks
	 *  to the pushNextStateTo method in SparseVector */
	public FlexCompColMatrix copyAsColMatrix() throws Exception {
		FlexCompColMatrix newColM = new FlexCompColMatrix(numRows, numColumns);
		// copy data
		int numRows = numRows();
		for ( int r = 0; r < numRows; r++ ) {
			SparseVector row = getRow(r);
			for ( int x = 0; x < row.used; x++ ) {
				double val = row.data[x];
				if ( val != 0 ) {
					// index[x] in row is the index of the column in the matrix
					int c = row.index[x];
					newColM.colD[c].pushNextStateTo(newColM.colD[c], r, val);
				}
			}
		}
		return newColM;
	}

	public SparseFlexCompColMatrix copyAsSparseColMatrix() throws Exception {
		SparseFlexCompColMatrix newColM = new SparseFlexCompColMatrix(numRows, numColumns);
		// copy data
		int numRows = numRows();
		for ( int r = 0; r < numRows; r++ ) {
			SparseVector row = getRow(r);
			for ( int x = 0; x < row.used; x++ ) {
				double val = row.data[x];
				if ( val != 0 ) {
					// index[x] in row is the index of the column in the matrix
					int c = row.index[x];
					newColM.colD[c].pushNextStateTo(newColM.colD[c], r, val);
				}
			}
		}
		return newColM;
	}

    @Override
    public FlexCompRowMatrix zero() {
        for (int i = 0; i < numRows; ++i)
            rowD[i].zero();
        return this;
    }

    /**
     * Computes the largest element in the matrix.<b>This is not an absolute
     * value!<b>
     * @return
     */
    public double maxValue() {
        double max = Double.NEGATIVE_INFINITY;
        for (int i = 0; i < numRows; ++i) {
            double tmp = rowD[i].max();
            if ( tmp > max ) max = tmp;
        }
        return max;
    }

    /**
     * Computes the smallest element in the matrix.<b>This is not an absolute
     * value!<b>
     * @return min(this)
     */
    public double minValue() {
        double min = Double.POSITIVE_INFINITY;
        for (int i = 0; i < numRows; ++i) {
            double tmp = rowD[i].min();
            if ( tmp < min ) min = tmp;
        }
        return min;
    }
    
    @Override
    public Matrix set(Matrix B) {
        if (!(B instanceof FlexCompRowMatrix))
            return super.set(B);

        checkSize(B);

        FlexCompRowMatrix Bc = (FlexCompRowMatrix) B;

        for (int i = 0; i < numRows; ++i)
            rowD[i].set(Bc.rowD[i]);

        return this;
    }

    /**
     * Tries to store the matrix as compactly as possible.
     * @see SparseVector#compact() 
     */
    public void compact() {
        for (Vector v : rowD)
            ((SparseVector) v).compact();
    }

    /**
     * @see SparseVector#hideExplicitZeros()
     */
    public void hideExplicitZeros() {
        for (Vector v : rowD)
            ((SparseVector) v).hideExplicitZeros();
    }

    /**
     * @see SparseVector#hideSmallValues(double)
     */
    public void hideSmallValues(double epsilon) {
        for (Vector v : rowD)
            ((SparseVector) v).hideSmallValues(epsilon);
    }
    
    /**
     * @see SparseVector#explicitZerosExist() 
     */
    public boolean explicitZerosExist() {
        for (Vector v : rowD) {
            if ( ((SparseVector) v).explicitZerosExist() ) {
                return true;
            }
        }
        return false;
    }
    
    /**
     * Iterator over a matrix stored vectorwise by rows
     */
    private class RowMatrixIterator implements Iterator<MatrixEntry>, Serializable {

        /**
         * Iterates over each row vector
         */
        private SuperIterator<SparseVector, VectorEntry> iterator = new SuperIterator<SparseVector, VectorEntry>(
                Arrays.asList(rowD));

        /**
         * Entry returned
         */
        private RowMatrixEntry entry = new RowMatrixEntry();

        public boolean hasNext() {
            return iterator.hasNext();
        }

        public MatrixEntry next() {
            SuperIteratorEntry<VectorEntry> se = iterator.next();
            entry.update(se.index(), se.get());
            return entry;
        }

        public void remove() {
            iterator.remove();
        }

    }

    /**
     * Entry of a matrix stored vectorwise by rows
     */
    private static class RowMatrixEntry implements MatrixEntry, Serializable {

        private int row;

        private VectorEntry entry;

        public void update(int row, VectorEntry entry) {
            this.row = row;
            this.entry = entry;
        }

        public int row() {
            return row;
        }

        public int column() {
            return entry.index();
        }

        public double get() {
            return entry.get();
        }

        public void set(double value) {
            entry.set(value);
        }

    }

}
