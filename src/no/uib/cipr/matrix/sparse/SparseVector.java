/*
 * Copyright (C) 2003-2006 Bjørn-Ove Heimsund
 * 
 * This file is part of MTJ.
 * 
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

package no.uib.cipr.matrix.sparse;

import java.io.Serializable;
import java.util.Iterator;

import no.uib.cipr.matrix.*;

/**
 * Sparse vector
 */
public class SparseVector extends AbstractVector implements ISparseVector, Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * Data
     */
    double[] data;

    /**
     * Indices to data
     */
    int[] index;

    /**
     * How much has been used
     */
    int used;

    /**
     * Constructor for SparseVector.
     * 
     * @param size
     *            Size of the vector
     * @param nz
     *            Initial number of non-zeros
     */
    public SparseVector(int size, int nz) {
        super(size);
        data = new double[nz];
        index = new int[nz];
    }

    /**
     * Make a copy of the data, but copy only used entries.
     * @param x
     */
    public SparseVector(SparseVector x) {
        super(x.size);
        
        used = x.used;
        data = new double[x.used];
        System.arraycopy(x.data, 0, data, 0, x.used);
        index = new int[x.used];
        System.arraycopy(x.index, 0, index, 0, x.used);
    }
    
    /**
     * Constructor for SparseVector, and copies the contents from the supplied
     * vector.
     * 
     * @param x
     *            Vector to copy from
     * @param deep
     *            True if a deep copy is to be made. If the copy is shallow,
     *            <code>x</code> must be a <code>SparseVector</code>
     */
    public SparseVector(Vector x, boolean deep) {
        super(x);

        if (deep) {
            int nz = Matrices.cardinality(x);
            data = new double[nz];
            index = new int[nz];
            set(x);
        } else {
            SparseVector xs = (SparseVector) x;
            data = xs.getData();
            index = xs.getIndex();
            used = xs.getUsed();
        }
    }

    /**
     * A uniform distribution over all states.
     */
    public void uniform() {
        if ( data.length < size ) {
            data = new double[size];
        }
        if ( index.length < size ) {
            index = new int[size];
        }
        used = size;
        for ( int i = 0; i < used; i++ ) {
            index[i] = i;
            data[i] = 1.0/(double)used;
        }
    }

	/**
	 * Increase the size of the sparse vector. For reducing the size another method truncate would need to be implemented.
	 * @param newSize
	 */
	public void incsize(int newSize) throws Exception {
		if ( size < newSize ) {
			size = newSize;
		} else {
			throw new Exception("the new size " + newSize + " has to be higher than the current size " + size);
		}
	}

    /**
     * Takes all states in x and puts them at the beginning of this
     * vector. All states in x have to be lower than states already in this.
     * @param x
     * @throws Exception 
     */
    public void insertPrefixFrom(SparseVector x) throws Exception {
        if ( MTJRuntimeConfig.MTJSLOWDEBUG ) {
            for ( int i = 0; i < this.used; i++ ) {
                for ( int j = 0; j < x.used; j++ ) {
                    if ( this.index[i] <= x.index[j] ) {
                        throw new Exception("all states in vector x have to be"
                                + " lower then any state in this vector");
                    }
                }
            }
        }
        if ( this.size < x.size ) {
            throw new Exception("initialised vector is too short");
        }
        int[] newIndes = new int[ this.used + x.used ];
        double[] newData = new double[ this.used + x.used ];
        System.arraycopy(x.index, 0, newIndes, 0, x.used);
        System.arraycopy(x.data, 0, newData, 0, x.used);
        System.arraycopy(this.index, 0, newIndes, x.used, this.used);
        System.arraycopy(this.data, 0, newData, x.used, this.used);
        data = newData;
        index = newIndes;
        used = this.used + x.used;
    }
    
    /**
     * Append the content of vector x at the end of vector this. All
     * states of vector x will be shifted to [index[i]+xIndexShift]. Normally
     * xIndexShift should be equal to the id of the highest state currently in x.
     * @param x
     * @param xIndexShift could be the number of states (numStates)
     * @throws Exception 
     */
    public void appendFrom(SparseVector x, int xIndexShift) throws Exception {
        if ( this.size < x.size + this.used ) {
            throw new Exception("initialised vector is too short");
        }
        for ( int i = 0; i < x.used; i++ ) {
            this.pushNextStateTo(this, x.index[i] + xIndexShift, x.data[i]);
        }
    }

	// protected double[] _tmpData;
	// protected int[] _tmpIndex;
	/**
	 * Append x at the end of vector this ignoring all zero entries in x. Indexes of elements of x will be shifted
	 * according to xIndexShift. This cannot have any values for index starting from xIndexShift when this function
	 * is executed.
	 * @param xIndexShift
	 * @param x
	 * @return
	 * @throws Exception
	 */
	public SparseVector appendFrom(int xIndexShift, double[] x) throws Exception {
		if ( this.size < x.length + this.used ) {
			throw new Exception("initialised vector is too short");
		}

		for ( int i = 0; i < x.length; i++ ) {
			this.pushNextStateTo(this, i + xIndexShift, x[i]);
		}

		return  this;

		/* // The code below was not good on fourth and cit: I could make it more efficient by removing _tmpData and
		   // assign values to this.data while counting non-zeros.
		// count the number of non-zero elements in x
		int xlen = x.length;
		if ( _tmpData == null || _tmpData.length < xlen ) {
			_tmpData = new double[xlen];
		}
		if ( _tmpIndex == null || _tmpIndex.length < xlen ) {
			_tmpIndex = new int[xlen];
		}
		int numTmp = 0;
		for ( int i = 0; i < xlen; i++ ) {
			if ( x[i] != 0 ) {
				_tmpIndex[numTmp] = xIndexShift + i;
				_tmpData[numTmp++] = x[i];
			}
		}
		// append new data at the end of the vector
		if ( data.length < used + numTmp ) {
			double[] newData = new double[used + numTmp];
			int[] newIndex = new int[used + numTmp];
			System.arraycopy(data, 0, newData, 0, used);
			System.arraycopy(index, 0, newIndex, 0, used);
			data = newData;
			index = newIndex;
		}
		System.arraycopy(_tmpData, 0, data, used, numTmp);
		System.arraycopy(_tmpIndex, 0, index, used, numTmp);
		used = used + numTmp;

		return  this; */
	}

    /**
     * Constructor for SparseVector, and copies the contents from the supplied
     * vector. Zero initial pre-allocation
     *
     * @param x
     *            Vector to copy from. A deep copy is made
     */
    public SparseVector(Vector x) {
        this(x, true);
    }

    /**
     * Constructor for SparseVector. Zero initial pre-allocation
     * 
     * @param size
     *            Size of the vector
     */
    public SparseVector(int size) {
        this(size, 0);
    }

    /**
     * Constructor for SparseVector
     * 
     * @param size
     *            Size of the vector
     * @param index
     *            Indices of the vector
     * @param data
     *            Entries of the vector
     * @param deep
     *            True for a deep copy. For shallow copies, the given indices
     *            will be used internally
     */
    public SparseVector(int size, int[] index, double[] data, boolean deep) {
        super(size);

        if (index.length != data.length)
            throw new IllegalArgumentException("index.length != data.length");

        if (deep) {
            used = index.length;
            this.index = index.clone();
            this.data = data.clone();
        } else {
            this.index = index;
            this.data = data;
            used = index.length;
        }
    }

	/** uses only len first positions from index and data; useful when vectors are created from the same set of arrays */
	public SparseVector(int size, int[] index, double[] data, int len) {
		super(size);

		if ( index.length < len || data.length < len || len > size )
			throw new IllegalArgumentException("wrong arguments");

		used = len;
		this.index = new int[len];
		this.data = new double[len];
		System.arraycopy(index, 0, this.index, 0, len);
		System.arraycopy(data, 0, this.data, 0, len);
	}

	/**
	 * Constructor that takes a dense array and converts it to the sparse representation.
	 * @param size
	 * @param dense
	 * @param initUsed predicted number of non-zeros in dense; useful for faster initialisation of the vector
	 * @throws Exception
	 */
	public SparseVector(int size, double[] dense, int initUsed) throws Exception{
		super(size);

		if ( dense.length > size ) throw new IllegalArgumentException("wrong arguments");

		if ( initUsed > size ) initUsed = size;

		used = 0;
		this.index = new int[initUsed];
		this.data = new double[initUsed];
		for ( int i = 0; i < dense.length; i++ ) {
			if ( dense[i] != 0 ) this.pushNextStateTo(this, i, dense[i]);
		}
	}

    /**
     * Constructor for SparseVector
     * 
     * @param size
     *            Size of the vector
     * @param index
     *            The vector indices are copies from this array
     * @param data
     *            The vector entries are copies from this array
     */
    public SparseVector(int size, int[] index, double[] data) {
        this(size, index, data, true);
    }

    @Override
    public void set(int index, double value) {
        check(index);

        // check against zero when setting zeros
        if ( value == 0.0 ) {
            final int existingI = checkIfIndexExists(index);
            if ( existingI == -1 ) {
                // nothing needs to be done because this index does not exist
                return;
            } else {
                // need to reset the value to zero because the index was found
                data[existingI] = 0.0;
                return;
            }
        }
        
        int i = getIndex(index);
        data[i] = value;
    }

    @Override
    public void add(int index, double value) {
        check(index);

        int i = getIndex(index);
        data[i] += value;
    }

    /**
	 * This method is not sparse, and it can be very slow. A sparse version is below. This function is very useful for
	 * testing and debugging.
     * this = this + alpha * y
     * @param alpha
     * @param y
     * @return this
	 * @see #add(double, SparseVector)
     */
    public SparseVector addNotSparse(double alpha, SparseVector y) {
        checkSize(y);

        if (alpha == 0) return this;

        for ( int yi = 0; yi < y.used; yi++ ) {
            this.add(y.index[yi], alpha * y.data[yi]);
        }

        return this;
    }

	/**
	 * Object this and v have to be compatible, i.e., they have non-zeros in the same places! This method was
	 * developed for POMDPs.
	 * @param v
	 * @return
	 */
    public double euclidDistCompatible(SparseVector v) throws Exception {
		if ( this.used != v.used ) throw new Exception(this.toStr() + " and " + v.toStr() + " are not compatible!");

		double sum = 0;

		for ( int i = 0; i < this.used; i++ ) {
			sum += Math.pow(this.data[i] - v.data[i], 2);
			if ( this.index[i] != v.index[i] ) throw new Exception(this.toStr() + " and " + v.toStr() + " are not compatible!");
		}

		return Math.sqrt(sum);
    }

	/**
	 * This function is fully sparse. Computes: this = this + alpha * v
	 * @param alpha
	 * @param v
	 * @return this
	 */
	public SparseVector add(double alpha, SparseVector v) throws Exception {

		if ( MTJRuntimeConfig.MTJDEBUG && this.size != v.size ) {
			throw new IndexOutOfBoundsException("x.size != v.size() (" + this.size + " != " + v.size + ")");
		}

		int i1 = 0;
		int i2 = 0;
		while ( i2 < v.used ) {
			// when both indexes exist
			if (i1 < this.used && this.index[i1] == v.index[i2]) {
				this.data[i1] += (alpha * v.data[i2]);
				i1++;
				i2++;
				continue;
			} else if ( i1 < this.used && this.index[i1] > v.index[i2] ) {
				if ( v.data[i2] != 0 ) {
					// i1 points further than i2; v[i2] is an element which is missing in this vector; we need to add v[i2] to this vector;
					// all elements i1 till the end will be shifted to i1+1, and the new element will be put in position i1
					this.insertStateAtPosition(v.index[i2], (alpha * v.data[i2]), i1);
					i1++; // adjust i1 because i2 went to i1
				}
				i2++;
				continue;
		    } else if ( i1 >= this.used ) {
				if ( v.data[i2] != 0 ) {
					// when i1 is beyond used indexes, we can just push the element at the end of this vector
					this.pushNextStateTo(this, v.index[i2], (alpha * v.data[i2]));
					i1++; // increase i1 so that it exceeds allowed indexes
				}
				i2++;
				continue;
			}
			if (i1 < this.used && this.index[i1] < v.index[i2] ) {
				i1++;
				continue;
			}
		}

		return this;
	}

    /**
     * Adds value to data(index(id))) and sets the result to 0.0 if the result
     * is negative.
     * @param id
     * @param value 
     */
    public void addAndMakeNonNegative(int id, double value) {
        check(id);

        int i = getIndex(id);
        data[i] += value;
        if ( data[i] < 0.0 ) {
            data[i] = 0.0;
        }
    }
    
    @Override
    public double get(int index) {
        if ( MTJRuntimeConfig.MTJDEBUG ) {
            check(index);
        }

        int in = Arrays.binarySearch(this.index, index, 0, used);
        if (in >= 0)
            return data[in];
        return 0;
    }

    
    /**
     * Checks if the index is in indexes.
     * @param ind
     * @return -1 when not found, the index value otherwise
     */
    public int checkIfIndexExists(int ind) {

        // Try to find column index
        int i = Arrays.binarySearchGreater(index, ind, 0, used);

        if (i < used && index[i] == ind) {
            return i;
        }
        return -1;
    }
    
    /**
     * This method is for manual construction of the sparse vector. It
     * assumes that state is higher then all elements in index. The next
     * state will be pushed to vector vec given as a parameter.
     * This method is very important because it allows to build a vector element
     * by element in linear time. The use of set() requires log(n) to find the
     * index.
     * @param vec
     * @param state
     * @param value 
     */
    public SparseVector pushNextStateTo(SparseVector vec, int state, double value) throws Exception {
        if ( MTJRuntimeConfig.MTJSLOWDEBUG ) {
            for ( int s : vec.index ) {
                if ( s > state ) {
                    throw new Exception("ERROR: when the vector is constructed"
                            + " manually " + state + " has to be higher then all"
                            + "indexes already in the sparse vector");
                }
            }
        }
        if ( value != 0 ) {
            if ( ++vec.used < vec.data.length ) {
                vec.data[vec.used - 1] = value;
                vec.index[vec.used - 1] = state; // s is sorted so vec.index will be fine
            } else {
                // allocate twice as much but no more than the overall size of the vector
                int newLength = vec.data.length != 0 ? Math.min( vec.data.length << 1, vec.size()) : 1;
                // Copy existing data into new arrays
                int[] newIndex = new int[newLength];
                double[] newData = new double[newLength];
                System.arraycopy(vec.index, 0, newIndex, 0, vec.index.length);
                System.arraycopy(vec.data, 0, newData, 0, vec.data.length);
                vec.data = newData;
                vec.index = newIndex;
                vec.data[vec.used - 1] = value;
                vec.index[vec.used - 1] = state; // s is sorted so y.index will be fine
            }
        }
        
        return vec;
    }
    
    /**
     * Tries to find the index. If it is not found, a reallocation is done, and
     * a new index is returned.
     */
    private int getIndex(int ind) {

        // Try to find column index
        int i = Arrays.binarySearchGreater(index, ind, 0, used);

        // Found
        if (i < used && index[i] == ind)
            return i;

		// ----------------------------
		// this block inserts a new element (with a value of zero) for state ind, at position i
        int[] newIndex = index;
        double[] newData = data;

        // Check available memory
        if (++used > data.length) {

            // If zero-length, use new length of 1, else double the bandwidth
            int newLength = data.length != 0 ? data.length << 1 : 1;

            // Copy existing data into new arrays
            newIndex = new int[newLength];
            newData = new double[newLength];
            System.arraycopy(index, 0, newIndex, 0, i);
            System.arraycopy(data, 0, newData, 0, i);
        }

        // All ok, make room for insertion
        System.arraycopy(index, i, newIndex, i + 1, used - i - 1);
        System.arraycopy(data, i, newData, i + 1, used - i - 1);

        // Put in new structure
        newIndex[i] = ind;
        newData[i] = 0.0;

        // Update pointers
        index = newIndex;
        data = newData;
		// ----------------------------

        // Return insertion index
        return i;
    }

	/**
	 * Insert a new state at internal position. The state is new when it was not stored in the vector. All positions
	 * from the current position have to be moved to position + 1; the new state is inserted at position position;
	 * @param state
	 * @param value
	 * @param position - this is an internal position of the sparse vector; it can be found using, e.g.
	 *                 int i = Arrays.binarySearchGreater(index, ind, 0, used); @see #getIndex()
	 * @return this
	 */
	private SparseVector insertStateAtPosition(int state, double value, int position) {

		// ----------------------------
		// this block inserts a new element for state "state", at position "position"
		int[] newIndex = index;
		double[] newData = data;

		// Check available memory
		if (++used > data.length) {

			// If zero-length, use new length of 1, else double the bandwidth
			int newLength = data.length != 0 ? data.length << 1 : 1;

			// Copy existing data into new arrays
			newIndex = new int[newLength];
			newData = new double[newLength];
			System.arraycopy(index, 0, newIndex, 0, position);
			System.arraycopy(data, 0, newData, 0, position);
		}

		// All ok, make room for insertion
		System.arraycopy(index, position, newIndex, position + 1, used - position - 1);
		System.arraycopy(data, position, newData, position + 1, used - position - 1);

		// Put in new structure
		newIndex[position] = state;
		newData[position] = value;

		// Update pointers
		index = newIndex;
		data = newData;
		// ----------------------------

		return this;
	}

    @Override
    public SparseVector copy() {
        return new SparseVector(this);
    }

    @Override
    public SparseVector zero() {
        java.util.Arrays.fill(data, 0);
		used = 0;
        return this;
    }

    @Override
    public SparseVector scale(double alpha) {
        // Quick return if possible
        if (alpha == 0)
            return zero();
        else if (alpha == 1)
            return this;

        for (int i = 0; i < used; ++i)
            data[i] *= alpha;

        return this;
    }

	/** this = this * (1/sum(this)) */
	public SparseVector normalise() throws Exception {

		double sum = 0;
		for (int i = 0; i < used; ++i)
			sum += data[i];

		if ( sum != 0 ) {
			for (int i = 0; i < used; ++i)
				data[i] /= sum;
		} else {
			throw new Exception(this.toStr() + "\nthis is a zero vector; normalisation cannot be performed");
		}

		return this;
	}

    @Override
    public double dot(Vector y) {
        if (!(y instanceof DenseVector))
            return super.dot(y);

        checkSize(y);

        double[] yd = ((DenseVector) y).getData();

        double ret = 0;
        for (int i = 0; i < used; ++i)
            ret += data[i] * yd[index[i]];
        return ret;
    }

    /**
     * This dot product is very useful when alpha vectors are double[].
     * TODO: I could create another dot product that would
     * take also the nonZeroR array and iterate over non-zero entries only.
     * @param y
     * @return 
     */
    public double dot(double[] y) {

        if ( MTJRuntimeConfig.MTJDEBUG && size != y.length) {
            throw new IndexOutOfBoundsException("x.size != y.length (" + size
                    + " != " + y.length + ")");
        }
        
        double ret = 0;
        for (int i = 0; i < used; ++i)
            ret += data[i] * y[index[i]];
        return ret;
    }

    /**
     * A linear time, in the number of stored elements, dot product between
     * two sparse vectors.
     * TODO: I should check how this was implemented in HSVI2, did they have
     * something better there?
     * @param v
     * @return 
     */
    public double dot(SparseVector v) {

        if ( MTJRuntimeConfig.MTJDEBUG && this.size != v.size ) {
            throw new IndexOutOfBoundsException("x.size != v.size() (" + this.size
                    + " != " + v.size + ")");
        }
        
        double ret = 0;
        int i1 = 0;
        int i2 = 0;
        while ( i1 < this.used && i2 < v.used ) {
            if ( this.index[i1] == v.index[i2] ) {
                ret += data[i1] * v.data[i2];
                i1++;
                i2++;
                continue;
            }
            if ( this.index[i1] < v.index[i2] ) {
                i1++;
                continue;
            }
            if ( this.index[i1] > v.index[i2] ) {
                i2++;
                continue;
            }
        }
        return ret;
    }
    
    /**
     * Computes: [ this += V1 - alpha * V2 ]. "this" can be longer than both 
     * arguments when only values of max the first V1.size states will be
	 * This function is quite slow because it relies on this.add(state,value).
     * modified.
     * @param V1
     * @param alpha
     * @param V2
     * @return this
     */
    public SparseVector addDiffScaleNotSparse(SparseVector V1, double alpha, SparseVector V2 ) {

        if ( MTJRuntimeConfig.MTJDEBUG && V1.size != V2.size ) {
            throw new IndexOutOfBoundsException("V1.size != V2.size() (" + V1.size
                    + " != " + V2.size + ")");
        }
            
        // iterate over non-zero entries in either of the two vectors
        int i1 = 0;
        int i2 = 0;
        while ( i1 < V1.used || i2 < V2.used ) {
            // no more entires in V2
            if ( i2 >= V2.used ) {
                double value = V1.data[i1];
                if ( value != 0 ) {
                    this.add(V1.index[i1], value);
                }
                i1++;
                continue;
            }
            // no more entries in V1
            if ( i1 >= V1.used ) {
                double value = 0.0 - alpha * V2.data[i2];
                if ( value != 0 ) {
                    this.add(V2.index[i2], value);
                }
                i2++;
                continue;
            }
            // both agree on the state
            if ( V1.index[i1] == V2.index[i2] ) {
                double value = V1.data[i1]  - alpha * V2.data[i2];
                if ( value != 0 ) {
                    this.add(V1.index[i1], value);
                }
                i1++;
                i2++;
                continue;
            }
            // V1 has a non-zero entry first
            if ( V1.index[i1] < V2.index[i2] ) {
                double value = V1.data[i1];
                if ( value != 0 ) {
                    this.add(V1.index[i1], value);
                }
                i1++;
                continue;
            }
            // V2 has a non-zero entry first
            if ( V1.index[i1] > V2.index[i2] ) {
                double value = 0.0 - alpha * V2.data[i2];
                if ( value != 0 ) {
                    this.add(V2.index[i2], value);
                }
                i2++;
                continue;
            }
        }

        return this;
    }

	/**
	 * [ this += v1 - alpha * v2 ]
	 * @param v1
	 * @param alpha
	 * @param v2
	 * @return this
	 * @throws Exception
	 */
	public SparseVector addDiffScale(SparseVector v1, double alpha, SparseVector v2) throws Exception {

		// use sparse addition twice, sparse addition is very efficient
		this.add(1.0, v1);
		this.add(-alpha, v2);

		return this;
	}

	/**
	 * @param V1
	 * @param V2
	 * @return max absolute difference between any entries in V1 and V2
	 */
	static public double maxAbsDiff(SparseVector V1, SparseVector V2 ) {

		if ( MTJRuntimeConfig.MTJDEBUG && V1.size != V2.size ) {
			throw new IndexOutOfBoundsException("V1.size != V2.size() (" + V1.size
					+ " != " + V2.size + ")");
		}

		double res = 0;

		// iterate over non-zero entries in either of the two vectors
		int i1 = 0;
		int i2 = 0;
		while ( i1 < V1.used || i2 < V2.used ) {
			// no more entires in V2
			if ( i2 >= V2.used ) {
				double value = V1.data[i1];
				if ( value != 0 ) {
					res = Math.max(res, Math.abs(value - 0.0));
				}
				i1++;
				continue;
			}
			// no more entries in V1
			if ( i1 >= V1.used ) {
				double value = V2.data[i2];
				if ( value != 0 ) {
					res = Math.max(res, Math.abs(value - 0.0));
				}
				i2++;
				continue;
			}
			// both agree on the state
			if ( V1.index[i1] == V2.index[i2] ) {
				double value = V1.data[i1]  - V2.data[i2];
				if ( value != 0 ) {
					res = Math.max(res, Math.abs(value));
				}
				i1++;
				i2++;
				continue;
			}
			// V1 has a non-zero entry first
			if ( V1.index[i1] < V2.index[i2] ) {
				double value = V1.data[i1];
				if ( value != 0 ) {
					res = Math.max(res, Math.abs(value - 0.0));
				}
				i1++;
				continue;
			}
			// V2 has a non-zero entry first
			if ( V1.index[i1] > V2.index[i2] ) {
				double value = V2.data[i2];
				if ( value != 0 ) {
					res = Math.max(res, Math.abs(value));
				}
				i2++;
				continue;
			}
		}

		return res;
	}

    /**
     * Computes: [ this += V1 - alpha * V2 ]. "this" can be longer than both 
     * arguments when only values of max the first V1.size states will be
     * modified. Only entries of masked states are updated.
     * @param V1
     * @param alpha
     * @param V2
     * @param mask
     * @return this
     */
    public SparseVector addDiffScale(SparseVector V1, double alpha, SparseVector V2, SparseVectorMask mask ) throws Exception {

        if ( MTJRuntimeConfig.MTJDEBUG && V1.size != V2.size ) {
            throw new IndexOutOfBoundsException("V1.size != V2.size() (" + V1.size
                    + " != " + V2.size + ")");
        }
        
        int i1 = 0;
        int i2 = 0;
        int im = 0;
        // iterate over non-zero entries in either of the two vectors
        while ( i1 < V1.used || i2 < V2.used ) {
            // ignore one of the vectors when its index exceeds its used elements
            if ( i1 < V1.used && i2 < V2.used ) {
                while ( im < mask._used && mask._index[im] < V1.index[i1] && mask._index[im] < V2.index[i2] ) {
                    im++;
                }
            } else if ( i1 < V1.used ) {
                while ( im < mask._used && mask._index[im] < V1.index[i1] ) {
                    im++;
                }
            } else if ( i2 < V2.used ) {
                while ( im < mask._used && mask._index[im] < V2.index[i2] ) {
                    im++;
                }
            }
            if ( im >= mask._used ) {
                // all masked states were visited already
                break;
            }
            if ( i1 < V1.used && mask._index[im] > V1.index[i1] ) {
                // skip states in V1 that were removed by the mask
                i1++;
                continue;
            }
            if ( i2 < V2.used && mask._index[im] > V2.index[i2] ) {
                // skip states in V2 that were removed by the mask
                i2++;
                continue;
            }
            // here either V1 index or V2 index should be equal to im
            if ( MTJRuntimeConfig.MTJDEBUG ) {
                if ( i1 < V1.used ) {
                    if ( mask._index[im] != V1.index[i1]  ) {
                        throw new Exception("ERROR: wrong idexes here!");
                    }
                } else if ( i2 < V2.used ) {
                    if ( mask._index[im] != V2.index[i2] ) {
                        throw new Exception("ERROR: wrong idexes here!");
                    }
                } else {
                    throw new Exception("cannot be here");
                }
            }
            // use only V2 when V1 does not have any more elements
            if ( i1 >= V1.used ) {
                double value = 0.0 - alpha * V2.data[i2];
                if ( value != 0 ) {
                    this.add(V2.index[i2], value);
                }
                i2++;
                continue;
            }
            // use only V1 when V2 does not have any more elements
            if ( i2 >= V2.used ) {
                double value = V1.data[i1];
                if ( value != 0 ) {
                    this.add(V1.index[i1], value);
                }
                i1++;
                continue;
            }
            // use both when both indexes agreed with the mask
            if ( V1.index[i1] == V2.index[i2] ) {
                double value = V1.data[i1]  - alpha * V2.data[i2];
                if ( value != 0 ) {
                    this.add(V1.index[i1], value);
                }
                i1++;
                i2++;
                continue;
            }
            // the first one agreed with the mask
            if ( V1.index[i1] < V2.index[i2] ) {
                double value = V1.data[i1];
                if ( value != 0 ) {
                    this.add(V1.index[i1], value);
                }
                i1++;
                continue;
            }
            // the second one agreed with the mask
            if ( V1.index[i1] > V2.index[i2] ) {
                double value = 0.0 - alpha * V2.data[i2];
                if ( value != 0 ) {
                    this.add(V2.index[i2], value);
                }
                i2++;
                continue;
            }
        }

        return this;
    }

	/**
	 * Computes: [ this -= alpha * other ] but other.compatibleWith(this) has to be true; needs to be checked before
	 * minusScaledCompatible2NonNeg is executed. Method useful for upper bound approximation in POMDPs. If the scaled
	 * value is negative, set it to zero.
	 * @param alpha
	 * @param other
	 * @return this
	 * @throws Exception
	 */
	public SparseVector minusScaledCompatible2NonNeg(double alpha, SparseVector other) throws Exception {

		if ( MTJRuntimeConfig.MTJDEBUG && this.size != other.size ) {
			throw new IndexOutOfBoundsException("this.size != other.size() (" + this.size + " != " + other.size + ")");
		}

		if ( alpha == 0 ) return this;

		int i = 0;
		for (int oi = 0; oi < other.used; ++oi) {
			// skip explicit zeros in other
			if ( other.data[oi] == 0) continue;
			// skip all positions in this that are not present in other
			while ( i < used && index[i] < other.index[oi] ) i++;
			if ( i >= used || other.index[oi] != index[i] ) throw new Exception(other.toStr() + " seems to be incompatible with " + this.toStr());
			// here we know that other.index[oi]==index[i] and that oi in other is non-zero
			if ( data[i] == 0 ) {
				throw new Exception(other.toStr() + " seems to be incompatible with " + this.toStr());
			} else {
				// can do this in constant time when beliefs are compatible
				data[i] -= alpha * other.data[oi];
				if ( data[i] < 0.0 ) data[i] = 0.0;
				i++;
			}
		}

		return this;
	}

    /**
     * Computes: [ this -= alpha * V ]. Only entries of masked states are updated.
     * <b>All new entries in this are made non-negative.</b>
     * @param alpha
     * @param V
     * @param mask
     * @return
     * @throws Exception 
     */
    public SparseVector minusScale(double alpha, SparseVector V, SparseVectorMask mask) throws Exception {

        if ( MTJRuntimeConfig.MTJDEBUG && this.size != V.size ) {
            throw new IndexOutOfBoundsException("this.size != V.size() (" + this.size
                    + " != " + V.size + ")");
        }
        
        if ( alpha == 0 ) return this;
        
        // iterate over non-zero entries of vector V
        int iv = 0;
        int im = 0;
        while ( iv < V.used ){
            while ( im < mask._used && mask._index[im] < V.index[iv] ) {
                im++;
            }
            if ( im >= mask._used ) {
                // all masked states were visited already
                break;
            }
            if ( mask._index[im] > V.index[iv] ) {
                // skip states in V that were removed by the mask
                iv++;
                continue;
            }
            // here V index should be equal to im
            if ( MTJRuntimeConfig.MTJDEBUG ) {
                if ( mask._index[im] != V.index[iv] ) {
                    throw new Exception("ERROR: wrong idexes here!");
                }
            }
            double value = alpha * V.data[iv];
            if ( value != 0 ) {
                this.addAndMakeNonNegative(V.index[iv], -value);
            }
            iv++;
        }

        return this;
    }        
    
    @Override public boolean equals(Object other) {
        if (!(other instanceof SparseVector)) {
            return false;
        }
        SparseVector otherVector = (SparseVector) other;
        if ( this.used != otherVector.used ) return false;
        // TODO: equal can be optimised, I will need to iterate over both
        // vectors at the same time;
        for (int i = 0; i < used; ++i) {
            // data[i] is the data stored in physical position i
            // index[i] is the external index in the vector that corresponds
            // to the physical position i
            if ( this.data[i] != otherVector.get(index[i]) ) return false;
        }
        return true;
    }

    public boolean equals(Object other, double tolerance) {
        if (!(other instanceof SparseVector)) {
            return false;
        }
        SparseVector otherVector = (SparseVector) other;
        if ( this.used != otherVector.used ) return false;
        for (int i = 0; i < used; ++i) {
            // data[i] is the data stored in physical position i
            // index[i] is the external index in the vector that corresponds
            // to the physical position i
            if ( Math.abs( this.data[i] - otherVector.get(index[i]) ) > tolerance ) return false;
        }
        return true;
    }

	/**
	 * We should be careful here what are the decimal places, but when values are probabilities, this is easy to predict.
	 * @param pruneToDecimalPlaces
	 */
	public void pruneToSigFigures(int pruneToDecimalPlaces) {
		for (int i = 0; i < used; ++i) {
			if ( this.data[i] != 0.0 ) {
				this.data[i] = pruneToSignificantFigures(this.data[i], pruneToDecimalPlaces);
			}
		}
	}

	/**
	 * Function copied from mgjcommon.CCommon
	 * @param num
	 * @param n
	 * @return
	 */
	public double pruneToSignificantFigures(double num, int n) {
		if(num == 0) {
			return 0;
		}

		final double d = Math.ceil(Math.log10(num < 0 ? -num: num));
		// without this check we cannot prune 0.00000000001234 to 5 digits correctly
		if ( -d > n ) {
			return 0.0;
		}
		final int power = n - (int) d;

		final double magnitude = Math.pow(10, power);
		if ( num > 0 ) {
			final double shifted = Math.floor(num*magnitude);
			return shifted/magnitude;
		} else {
			final double shifted = Math.ceil(num*magnitude);
			return shifted/magnitude;
		}
	}

    /**
     * This implementation takes only first N elements. This may not be good
     * in some cases. TODO: I need come up with a better hash function!
     * @return the hash code of the sparse vector
     */
    @Override public int hashCode() {
        double hash = used;
        final int N = 10;
        // take only first N elements
        for ( int i = 0; i < Math.min(used, N); i++ ) {
            if ( data[i] == 0 ) continue;
            hash += Double.valueOf(data[i]).hashCode() * 7 * ( i + 1);
        }
        return (int)(hash);
    }

    /**
     * Computes the sum of all elements in the vector.
     * @return 
     */
    public double sum() {
        double sum = 0;
        for (int i = 0; i < used; ++i)
            sum += data[i];
        return sum;
    }

    /**
     * Computes the sum of those elements in the vector that are contained
     * in the sparse vector mask.
     * @return 
     */
    public double sum(SparseVectorMask mask) throws Exception {
        double sum = 0;
        int mi = 0;
        for (int i = 0; i < used; ++i) {
            while ( mi < mask._used && mask._index[mi] < this.index[i] ) {
                mi++;
            }
            if ( mask._used <= mi ) {
                break;
            }
            if ( mask._index[mi] > this.index[i] ) {
                // increase i
                continue;
            }
            if ( mask._index[mi] == this.index[i] ) {
                // add data and then increase mi and i
                sum += data[i];
                mi++;
            } else {
                if ( MTJRuntimeConfig.MTJDEBUG ) {
                    throw new Exception("ERROR: very wrong here!");
                }
            }
        }
        return sum;
    }

    /**
     * I need to assume that some data[s] may be zeros stored explicitly.
     * Method used for computing upper bound approximations in POMDPs. <b>NB: this method is not symmetric!</b>
     * @param other
     * @return true when: forall_{s} this.data(s)!=0 only if other.data(s)!=0
     * @throws Exception
     */
    public boolean compatibleWith(SparseVector other) throws Exception {
        int oi = 0;
        for (int i = 0; i < used; ++i) {
            // skip explicit zeros in this
            if ( data[i] == 0) continue;
            // skip all positions in other (either explicit zero or not) that are not present in this
            while ( oi < other.used && other.index[oi] < index[i] ) oi++;
            // if i was not in other (and we know from above that i in this is non-zero) then false
            // or if oi is out of bound
            if ( oi >= other.used || other.index[oi] != index[i] ) return false;
            // here we know that other.index[oi]==index[i] and that i in this is non-zero
            // deal with explicit zeros
            if ( other.data[oi] == 0 ) {
                return false;
            } else {
                oi++;
            }
        }
        return true;
    }

	/**
	 * @return entropy of the sparse vector assuming that the vector stores a multinomial probability distribution; if
	 * the distribution is not normalised, the entropy is adjusted so that it corresponds to the normalised distribution;
	 */
	public double entropy() throws Exception {
		return entropy(this.sum());
	}

	/**
	 * Entropy computation where the sum of elements is provided as a parameter.
	 * @param sum
	 * @return
	 * @throws Exception
	 */
	public double entropy(final double sum) throws Exception {
		if ( MTJRuntimeConfig.MTJSLOWDEBUG ) {
			if ( sum != this.sum() ) throw new Exception("entropy() execution error; sum has to be the sum" +
					" of the vector elements; sum is " + sum + "; vector sum is " + sum() + "; vector is " + toStr());
		}
		if ( sum <= 0 ) throw new Exception("entropy computation failed on a zero vector");
		if ( used == 1) {
			return 0.0;
		} else {
			double entr = 0;
			for ( int i = 0; i < used; i++ ) {
				final double p = data[i] / sum;
				// 0*log(0) is zero and is not counted
				if ( p != 0 ) {
					entr -= p * Math.log( p );
				}
			}
			return entr;
		}
	}

    /**
     * Computes the smallest element in the vector.
     * @return 
     */
    public double min() {
        double min = Double.POSITIVE_INFINITY;
        for (int i = 0; i < used; ++i)
            min = Math.min(data[i], min);
        if ( used < size ) {
            min = Math.min(0.0, min);
        }
        return min;
    }

    /**
     * Computes the largest element in the vector. <b>This is not an absolute
     * value!<b>
     * @return 
     */
    public double max() {
        double max = Double.NEGATIVE_INFINITY;
        for (int i = 0; i < used; ++i)
            max = Math.max(data[i], max);
        if ( used < size ) {
            max = Math.max(0.0, max);
        }
        return max;
    }
    
    @Override
    public double norm1() {
        double sum = 0;
        for (int i = 0; i < used; ++i)
            sum += Math.abs(data[i]);
        return sum;
    }

    @Override
    public double norm2() {
        double norm = 0;
        for (int i = 0; i < used; ++i)
            norm += data[i] * data[i];
        return Math.sqrt(norm);
    }

    @Override
    public double norm2_robust() {
        double scale = 0, ssq = 1;
        for (int i = 0; i < used; ++i) {
            if (data[i] != 0) {
                double absxi = Math.abs(data[i]);
                if (scale < absxi) {
                    ssq = 1 + ssq * Math.pow(scale / absxi, 2);
                    scale = absxi;
                } else
                    ssq = ssq + Math.pow(absxi / scale, 2);
            }
        }
        return scale * Math.sqrt(ssq);
    }

    @Override
    public double normInf() {
        double max = 0;
        for (int i = 0; i < used; ++i)
            max = Math.max(Math.abs(data[i]), max);
        return max;
    }

    /**
     * Creates a copy of this vector as a dense array.
     * @return the new array with the copy of the data
     */
    public double[] getCopyAsDenseArray() {
        double[] res = new double[size];
        for ( int i = 0; i < used; i++ ) {
            res[index[i]] = data[i];
        }
        return res;
    }

	/**
	 * @return a boolean array that has true for every index of the sparse vector that has non-zero entry; a new array
	 * is allocated;
	 */
	public boolean[] getNonZeros() {
		// sparse entries and explicit zeros are false by default
		boolean[] res = new boolean[size];
		for ( int i = 0; i < used; i++ ) {
			if ( data[i] != 0.0 ) {
				res[index[i]] = true;
			}
		}
		return res;
	}

    /**
     * Creates a copy of this vector as a dense array. Only states 
     * contained in the mask are used. So, the returned belief state is over
     * the masked states only.
     * @return the new array with the copy of the masked data
     */
    public double[] getCopyAsDenseArray(SparseVectorMask mask) {
        double[] res = new double[mask._used];
        
        int im = 0;
        for ( int i = 0; i < used; i++ ) {
            while ( im < mask._used && mask._index[im] < this.index[i] ) {
                im++;
            }
            if ( im >= mask._used ) {
                // all masked states were visited already
                break;
            }
            if ( mask._index[im] > this.index[i] ) {
                // skip states in this that were removed by the mask
                continue;
            }
            res[im] = data[i];
        }
        
        return res;
    }
    
    /**
     * Puts the data of this vector in the given array.
     * @return array is returned
     */
    public double[] fillInDenseArray(double[] array) throws Exception {
        if ( MTJRuntimeConfig.MTJDEBUG ) {
            if ( this.size != array.length ) {
                throw new Exception("array has incompatible lengh; " + array.length +
                        " given but " + this.size + " was required");
            }
        }
        java.util.Arrays.fill(array, 0.0);
        for ( int i = 0; i < used; i++ ) {
            array[index[i]] = data[i];
        }
        return array;
    }

    /**
	 * <b>Use index.clone() when the full index has to be cloned.</b>
	 * @see #getIndex() if you need an entire index
     * @return Returns the indices. A new array is created when index.length > used. Otherwise raw data is returned.
     */
    public int[] getIndexCompressed() {
    	if (used == index.length)
    		return index;
    	
    	// could run compact, or return subarray
    	// compact();
    	int [] indices = new int[used];
    	for (int i = 0 ; i < used; i++) {
    		indices[i] = index[i];
    	}
    	return indices;
    }

	/**
	 * <b>The pointer is returned so changes will affect the vector.</b> The metnod gives access to raw indexes of the sparse vector.
	 * @see #getIndexCompressed() if you need indexes which are in use (copying index may be required)
	 * @return raw index data. The number of useful entries is determined by this.used.
	 */
	public int[] getIndex() {
		return index;
	}

	/**
	 * @return data when data.length == used; otherwise create a new compressed array and return it
	 */
	public double[] getDataCompressed() {
		if (used == data.length)
			return data;

		// could run compact, or return subarray
		// compact();
		double [] newData = new double[used];
		for (int i = 0 ; i < used; i++) {
			newData[i] = data[i];
		}
		return newData;
	}

	/**
	 * Returns the internal data. <b>Be careful, this data is indexed using
	 * internal indexes of this class. So, it is meaningless without
	 * this.index.</b>
	 */
	public double[] getData() {
		return data;
	}

    public SparseVectorMask getMask() {
        SparseVectorMask mask = new SparseVectorMask(used);
        mask._used = 0;
        int mi = 0;
        for ( int i = 0; i < used; i++ ) {
            // only non-zero elements are considered in the new mask index
            if ( data[i] != 0 ) {
                mask._index[mi++] = index[i];
                mask._used++;
            }
        }
    	return mask;
    }

	/**
	 * Get the number of elements that are not zero. Check all elements in case some explicit zeros are stored. If I
	 * was sure that zeros are never stored, I could return used directly.
	 * @return num
	 */
	public int getNumNonZeros() {
		int res = 0;
		for ( int i = 0; i < used; i++ ) {
			if ( data[i] != 0 ) {
				res++;
			}
		}
		return res;
	}

	/**
	 * @param tolerance
	 * @return the number of non-zero elements whose absolute values are higher than given tolerance
	 */
	public int getNumNonZeros(double tolerance) {
		int res = 0;
		for ( int i = 0; i < used; i++ ) {
			if ( Math.abs(data[i]) > tolerance ) {
				res++;
			}
		}
		return res;
	}

	/** @return true when there is more than one non-zero entry */
	public boolean moreThanOneNonZero() {
		int res = 0;
		for ( int i = 0; i < used; i++ ) {
			if ( data[i] != 0 ) {
				res++;
				if ( res > 1 ) return true;
			}
		}
		return false;
	}

    /**
     * Number of entries used in the sparse structure
     */
    public int getUsed() {
        return used;
    }

    /**
     * Compacts the vector. This process reduces memory consumption
     * as much as possible. After this operation, vector is sparse and uses
     * a minimal amount of memory.
     */
    public void compact() {
		int nz = 0;
		for ( int i = 0; i < used; i++ ) {
			if ( data[i] != 0 ) {
				nz++;
			}
		}

        if ( nz < data.length ) {
            int[] newIndex = new int[nz];
            double[] newData = new double[nz];

            // Copy only non-zero entries
            for (int i = 0, j = 0; i < used; ++i) {
                if ( data[i] != 0 ) {
                    newIndex[j] = index[i];
                    newData[j] = data[i];
                    j++;
                }
				if ( j == nz ) break;
			}

            data = newData;
            index = newIndex;
            used = data.length;
        }
    }

    /**
     * Makes the vector sparse by hiding zeros. Extra memory is not
     * freed in this method. This method is designed for speed. If memory should
     * be reduced as well, then the compact() method should be used instead.
     */
    public void hideExplicitZeros() {
        // keep only non-zero indexes
        int i = 0;
        int j = 0;
        while ( i < used ) {
            if ( data[i] != 0.0 ) {
                index[j] = index[i];
                data[j] = data[i];
                j++;
            }
            i++;
        }
        used = j;
    }

    /**
     * @see #hideExplicitZeros()
     * @param epsilon - the precision, all values abs(val) < epsilon or val < 0 will be removed;
     */
    public void hideSmallAndNegValues(double epsilon) {
        int i = 0;
        int j = 0;
        while ( i < used ) {
            if ( Math.abs(data[i]) > epsilon && data[i] > 0 ) {
                index[j] = index[i];
                data[j] = data[i];
                j++;
            }
            i++;
        }
        used = j;
    }

    /**
     * @see #hideExplicitZeros()
     * @param epsilon - the precision, all values abs(val) < epsilon will be removed;
    */
    public void hideSmallValues(double epsilon) {
        int i = 0;
        int j = 0;
        while ( i < used ) {
            if ( Math.abs(data[i]) > epsilon ) {
                index[j] = index[i];
                data[j] = data[i];
                j++;
            }
            i++;
        }
        used = j;
    }

	/** it should not be used when num is not a probability but a very large value */
	public double round(double num, double accuracy) {
		return Math.round(num / accuracy) * accuracy;
	}

	/** beliefs should be re-normalised after this operation */
	public void round2precision(double precision) {
		for ( int i = 0; i < used; i++ ) {
			data[i] = round(data[i], precision);
		}
	}

	/** round to the default precision of 0.5/size; beliefs should be re-normalised after this operation */
	public void round2precision() {
		double precision = 0.5 / size;
		for ( int i = 0; i < used; i++ ) {
			data[i] = round(data[i], precision);
		}
	}

    /**
     * @return true when at least one zero is stored explicitly in the matrix.
     */
    public boolean explicitZerosExist() {
        for ( int i = 0; i < used; i++ ) {
            if ( data[i] == 0.0 ) {
                return true;
            }
        }
        return false;
    }

    @Override
    public Iterator<VectorEntry> iterator() {
        return new SparseVectorIterator();
    }

    @Override
    public Vector set(Vector y) {
        if (!(y instanceof SparseVector))
            return super.set(y);

        checkSize(y);

        SparseVector yc = (SparseVector) y;

        if (yc.index.length != index.length) {
            data = new double[yc.data.length];
            index = new int[yc.data.length];
        }

        System.arraycopy(yc.data, 0, data, 0, data.length);
        System.arraycopy(yc.index, 0, index, 0, index.length);
        used = yc.used;

        return this;
    }

	public SparseVector setUsed(SparseVector y) {

		if (y.used > index.length) {
			data = new double[y.used];
			index = new int[y.used];
		}

		System.arraycopy(y.data, 0, data, 0, Math.min(data.length, y.used));
		System.arraycopy(y.index, 0, index, 0, Math.min(index.length, y.used));
		used = y.used;

		return this;
	}

	/**
	 * Initialises first positions of this vector using data given in x. Existing data is overwritten by data from x.
	 * This is deep initialisation.
	 */
	public void initFrom(SparseVector x) throws Exception {
		if ( this.size < x.size ) {
			throw new Exception("initialised vector is too short");
		}
		data = x.data.clone();
		index = x.index.clone();
		used = x.used;
	}

    /**
     * Iterator over a sparse vector
     */
    private class SparseVectorIterator implements Iterator<VectorEntry> {

        private int cursor;

        private final SparseVectorEntry entry = new SparseVectorEntry();

        public boolean hasNext() {
            return cursor < used;
        }

        public VectorEntry next() {
            entry.update(cursor);

            cursor++;

            return entry;
        }

        public void remove() {
            entry.set(0);
        }

    }

    /**
     * Entry of a sparse vector
     */
    private class SparseVectorEntry implements VectorEntry {

        private int cursor;

        public void update(int cursor) {
            this.cursor = cursor;
        }

        public int index() {
            return index[cursor];
        }

        public double get() {
            return data[cursor];
        }

        public void set(double value) {
            data[cursor] = value;
        }

    }

	// -----
	// Methods related to POMDPs

	/**
	 * A non-normalised corner can be checked using getNumNonZeros().
	 * @return return true when this is a normalised corner
	 */
	public boolean isCorner() {

		if ( getNumNonZeros() > 1 ) return false;

		// here, we know that there is only one non-zero element (there may be explicit zeros as well)
		for (int i = 0; i < used; ++i) {
			if ( data[i] != 1.0 && data[i] != 0.0 ) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Useful for computing occupancy frequency. Computes a dot product, but the product is for only those states
	 * whose policy[s] == action.
	 * @param y
	 * @param action
	 * @param policy
	 * @return this * y (for s, s.t. policy[s] == action)
	 */
	public double dot(double[] y, int action, int[] policy) {

		if ( MTJRuntimeConfig.MTJDEBUG && size != y.length) {
			throw new IndexOutOfBoundsException("x.size != y.length (" + size + " != " + y.length + ")");
		}

		double ret = 0;
		for (int i = 0; i < used; ++i) {
			if ( policy[index[i]] == action ) {
				ret += data[i] * y[index[i]];
			}
		}
		return ret;
	}

	/**
	 * Computes a dot product but only on those indexes that are in indexes. <b>When I compared this function with
	 * the one above, occupancy frequency computation of FIB policy on baseball.POMDP is faster with the one above.</b>
	 * @param y
	 * @param indexes
	 * @return this * y [ for s, s.t. exists i s.t. indexes[i] == s]
	 */
	public double dot(double[] y, int[] indexes) {

		if ( MTJRuntimeConfig.MTJDEBUG && size != y.length ) {
			throw new IndexOutOfBoundsException("x.size != y.length (" + size + " != " + y.length + ")");
		}

		if ( MTJRuntimeConfig.MTJDEBUG && indexes[indexes.length-1] >= size ) {
			throw new IndexOutOfBoundsException("wrong indexes array");
		}

		double ret = 0;
		final int nidx = indexes.length;

		int i1 = 0;
		int i2 = 0;
		while ( i1 < this.used && i2 < nidx ) {
			if ( this.index[i1] == indexes[i2] ) {
				ret += data[i1] * y[indexes[i2]];
				i1++;
				i2++;
				continue;
			}
			if ( this.index[i1] < indexes[i2] ) {
				i1++;
				continue;
			}
			if ( this.index[i1] > indexes[i2] ) {
				i2++;
				continue;
			}
		}

		return ret;
	}

	/**
	 * Computes c[b_i] that is required for the saw tooth approximation.
	 * It should be called from the object that represents belief_i and
	 * the parameter is the belief that the upper bound is computed for.
	 * When this is not compatible with belief, then 0.0 should be returned.
	 * Zero has to be returned because it means that belief is on the subspace
	 * of the belief simplex, whereas belief_i is on the full simplex hence
	 * belief_i is not relevant for the saw tooth of belief.
	 * It should be fine, as long as this=belief_i, and belief=query belief.
	 */
	public double computeC2(SparseVector belief) throws Exception {

		if ( MTJRuntimeConfig.MTJDEBUG && size != belief.size) {
			throw new IndexOutOfBoundsException("this.size != belief.length (" + size
					+ " != " + belief.size + ")");
		}

		double tmpc = Double.POSITIVE_INFINITY;
		int yi = 0;
		for ( int i = 0; i < used; i++ ) {
			double bis = data[i];
			if ( bis > 0 ) {
				// (1) - can do it through get in y
				// double tmp = belief.get(index[i]) / bis;
				// (2) - or directly without binary search in y's get
				while ( yi < belief.used && belief.index[yi] < index[i]) {
					yi++;
				}
				if ( yi >= belief.used ) {
					// here belief.get(index[i]) and all remaining are zero
					break;
				}
				double tmp = 0; // this is the case when y.index[yi] > index[i]
				if ( belief.index[yi] == index[i] ) {
					tmp = belief.data[yi] / bis;
					if ( MTJRuntimeConfig.MTJDEBUG ) {
						double val2 = belief.get(index[i]);
						if ( belief.data[yi] != val2 ) {
							throw new Exception("ERROR: wrong value of val= " + belief.data[yi] +
									" here! Should be val2=" + val2 + "\n " +
									this.toStr() + "\n" + belief.toStr() + "\n");
						}
					}
				}
				if ( tmp < tmpc ) tmpc = tmp;
			}
		}
		if ( MTJRuntimeConfig.MTJDEBUG && tmpc == Double.POSITIVE_INFINITY ) {
			throw new Exception("ERROR: wrong value here!");
		}
		return tmpc;
	}

	/**
	 * Computes c[b_i] that is required for the saw tooth approximation.
	 * It should be called from the object that represents a query belief
	 * and the parameter is the next evaluated belief from the belief set.
	 * "this" should be the belief we are computing the bound for.
	 * <b>If we want to make it work with incompatible beliefs, we will
	 * need to detect that the beliefI is not compatible with this and then
	 * return 0.0, because min will be 0.0/beliefI(s) for some s. The other
	 * way to do it for incompatible beliefI would be to make this function
	 * be called as beliefI.computeC(belief) and then iterating over non-zero
	 * values of beliefI would be sufficient, and 0.0/beliefI(s) will be
	 * properly found (see computeC2).</b>
	 */
	public double computeC(SparseVector beliefI) throws Exception {

		if ( MTJRuntimeConfig.MTJDEBUG && size != beliefI.size) {
			throw new IndexOutOfBoundsException("x.size != beliefI.length (" + size
					+ " != " + beliefI.size + ")");
		}

		double tmpc = Double.POSITIVE_INFINITY;
		int yi = 0;
		for ( int i = 0; i < used; i++ ) {
			double bs = data[i];
			// (1) - bis can be read through get
			// double bis = beliefI.get(index[i]);
			// (2) - or bis can be read directly without binary search in beliefI's get
			while ( yi < beliefI.used && beliefI.index[yi] < index[i] ) {
				yi++;
			}
			if ( yi >= beliefI.used ) {
				// here beliefI.get(index[i]) and all remaining are zero
				break;
			}
			double bis = 0; // this is the case when beliefI.index[yi] > index[i]
			if ( beliefI.index[yi] == index[i] ) {
				bis = beliefI.data[yi];
				if ( MTJRuntimeConfig.MTJDEBUG ) {
					double bis2 = beliefI.get(index[i]);
					if ( bis != bis2 ) {
						throw new Exception("ERROR: wrong value of bis= " + bis +
								" here! Should be bis2=" + bis2 + "\n " +
								this.toStr() + "\n" + beliefI.toStr() + "\n");
					}
				}
			}
			if ( bis > 0 ) {
				double tmp = bs / bis;
				if ( tmp < tmpc ) tmpc = tmp;
			}
		}

		if ( MTJRuntimeConfig.MTJDEBUG && tmpc == Double.POSITIVE_INFINITY ) {
			throw new Exception("ERROR: wrong here, perhaps " + beliefI.toStr() +
					"\n is not compatible with " + this.toStr() + "\n");
		}
		return tmpc;
	}
}
