/*
 * Copyright (C) 2003-2006 Bjørn-Ove Heimsund
 * 
 * This file is part of MTJ.
 * 
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

package no.uib.cipr.matrix.sparse;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Iterator;

import no.uib.cipr.matrix.*;
import no.uib.cipr.matrix.sparse.SuperIterator.SuperIteratorEntry;

/**
 * Matrix stored column-wise into sparse vectors
 */
public class FlexCompColMatrix extends AbstractMatrix implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * Matrix data
     */
    SparseVector[] colD;

    /**
     * Constructor for FlexCompColMatrix
     * 
     * @param numRows
     *            Number of rows
     * @param numColumns
     *            Number of column
     */
    public FlexCompColMatrix(int numRows, int numColumns) {
        super(numRows, numColumns);

        colD = new SparseVector[numColumns];
        for (int i = 0; i < numColumns; ++i)
            colD[i] = new SparseVector(numRows);
    }

	public FlexCompColMatrix(int numRows, int numColumns, boolean initColumns) {
		super(numRows, numColumns);

		colD = new SparseVector[numColumns];
		if ( initColumns ) {
			for (int i = 0; i < numColumns; ++i)
				colD[i] = new SparseVector(numRows);
		}
	}

    /**
     * Constructor for FlexCompColMatrix
     * 
     * @param A
     *            Matrix to copy contents from
     * @param deep
     *            True for a deep copy, false for a reference copy. A reference
     *            copy can only be made of an <code>FlexCompColMatrix</code>
     */
    public FlexCompColMatrix(Matrix A, boolean deep) {
        super(A);
        colD = new SparseVector[numColumns];

        if (deep) {
            for (int i = 0; i < numColumns; ++i)
                colD[i] = new SparseVector(numRows);
            set(A);
        } else {
            FlexCompColMatrix Ar = (FlexCompColMatrix) A;
            for (int i = 0; i < numColumns; ++i)
                colD[i] = Ar.getColumn(i);
        }
    }

    /**
     * Constructor for FlexCompColMatrix
     * 
     * @param A
     *            Matrix to copy contents from. The copy will be deep
     */
    public FlexCompColMatrix(Matrix A) {
        this(A, true);
    }

    /**
     * Returns the given column
     */
    public SparseVector getColumn(int i) {
        return colD[i];
    }

    /**
     * Sets the given column equal the passed vector
     */
    public void setColumn(int i, SparseVector x) {
        if (x.size() != numRows)
            throw new IllegalArgumentException(
                    "New column must be of the same size as existing column");
        colD[i] = x;
    }

    /**
     * y = A * x, where A is this matrix. Note that A is a column matrix,
     * and we need to multiply every row of A by x. The code below does a nice
     * linear time traversal through all the sparse data.
     * @param x
     * @return a new vector y is returned
     */
    public SparseVector mult(SparseVector x) throws Exception {
        SparseVector res = new SparseVector(numRows);
        
        int[] colIndexes = new int[numColumns];
        for ( int row = 0; row < numRows; row++ ) {
            double rowVal = 0.0;
            int xi = 0;
            for ( int col = 0; col < numColumns; col++ ) {
                while ( xi < x.used && x.index[xi] < col ) {
                    xi++;
                }
                if ( xi >= x.used || x.index[xi] > col ) {
                    continue;
                }
                while ( colIndexes[col] < colD[col].used && colD[col].index[colIndexes[col]] < row ) {
                    colIndexes[col]++;
                }
                if ( colIndexes[col] >= colD[col].used || colD[col].index[colIndexes[col]] > row ) {
                    continue;
                }
                // multiply only when indexes agree on the state, if not
                // then at least one of the values is zero and we can skip it
                if ( colD[col].index[colIndexes[col]] == row && x.index[xi] == col ) {
                    rowVal += colD[col].data[colIndexes[col]] * x.data[xi];
                }
            }
            if ( rowVal != 0 ) {
                res.pushNextStateTo(res, row, rowVal);
            }
        }
        
        if ( MTJRuntimeConfig.MTJSLOWDEBUG ) {
            SparseVector tmp = new SparseVector(numRows);
            this.mult(x, tmp);
            for ( int i = 0; i < numRows; i++ ) {
                if ( res.get(i) != tmp.get(i) ) {
                    throw new Exception("wrong values, it should be " + tmp.toStr() +
                            "\n but got " + res.toStr());
                }
            }
        }
        
        return res;
    }

	/**
	 * y = A * x, where A is this matrix. Note that A is a column matrix,
	 * and we need to multiply every row of A by x. The code below does a nice
	 * linear time traversal through all the sparse data.
	 * @see #mult(SparseVector)
	 * @param x
	 * @param colIndexes in order to avoid allocation in this method
	 * @return a new vector y is returned
	 */
	public SparseVector mult(SparseVector x, int[] colIndexes) throws Exception {
		SparseVector res = new SparseVector(numRows);

		Arrays.fill(colIndexes, 0);
		for ( int row = 0; row < numRows; row++ ) {
			double rowVal = 0.0;
			int xi = 0;
			for ( int col = 0; col < numColumns; col++ ) {
				while ( xi < x.used && x.index[xi] < col ) {
					xi++;
				}
				if ( xi >= x.used || x.index[xi] > col ) {
					continue;
				}
				while ( colIndexes[col] < colD[col].used && colD[col].index[colIndexes[col]] < row ) {
					colIndexes[col]++;
				}
				if ( colIndexes[col] >= colD[col].used || colD[col].index[colIndexes[col]] > row ) {
					continue;
				}
				// multiply only when indexes agree on the state, if not
				// then at least one of the values is zero and we can skip it
				if ( colD[col].index[colIndexes[col]] == row && x.index[xi] == col ) {
					rowVal += colD[col].data[colIndexes[col]] * x.data[xi];
				}
			}
			if ( rowVal != 0 ) {
				res.pushNextStateTo(res, row, rowVal);
			}
		}

		if ( MTJRuntimeConfig.MTJSLOWDEBUG ) {
			SparseVector tmp = new SparseVector(numRows);
			this.mult(x, tmp);
			for ( int i = 0; i < numRows; i++ ) {
				if ( res.get(i) != tmp.get(i) ) {
					throw new Exception("wrong values, it should be " + tmp.toStr() +
							"\n but got " + res.toStr());
				}
			}
		}

		return res;
	}

    /**
     * this = this(:,0) * y(0) + ... +  this(:,n) * y(n)
     * Very useful when the TAO matrix is created for POMDPs.
     * @param y
     */
    public void scale(SparseVector y) throws Exception {
        int yi = 0; // when y.used=0 then yi is meaningless
        for (int i = 0; i < numColumns; ++i) {
            if ( y.used == 0 || yi >= y.used || y.index[yi] > i ) {
                this.colD[i].zero(); // * y(i) == 0
            } else if ( yi < y.used && y.index[yi] == i ) {
                this.colD[i].scale(y.data[yi]);
                yi++;
            } else {
                if ( MTJRuntimeConfig.MTJDEBUG ) {
                    throw new Exception("cannot be here");
                }
            }
        }
    }
    
    @Override
    public Vector multAdd(double alpha, Vector x, Vector y) {
        if (!(x instanceof DenseVector) || !(y instanceof DenseVector))
            return super.multAdd(alpha, x, y);

        checkMultAdd(x, y);

        double[] xd = ((DenseVector) x).getData();
        double[] yd = ((DenseVector) y).getData();

        // y = 1/alpha * y
        y.scale(1. / alpha);

        // y = A*x + y
        for (int i = 0; i < numColumns; ++i) {
            SparseVector v = colD[i];
            int[] index = v.getIndex();
            double[] data = v.getData();
            int length = v.getUsed();
            for (int j = 0; j < length; ++j)
                yd[index[j]] += data[j] * xd[i];
        }

        // y = alpha*y = alpha * A'x + y
        return y.scale(alpha);
    }

	/**
	 * <b>This function has to be reimplemented</b> in the same way as: transMult in the same class.
	 * @param alpha
	 * @param x
	 * @param y
	 * @return y
	 */
    @Override
    public Vector transMultAdd(final double alpha, final Vector x,
            final Vector y) {
        checkTransMultAdd(x, y);

        for (int i = 0; i < numColumns; ++i)
            y.add(i, alpha * colD[i].dot(x));

        return y;
    }

    /**
	 * <b>Exploits sparsity.</b>
     * y=A^T*x where A is this matrix. This function constructs vector y
     * explicitly without using binary search on indexes of the y vector.
	 * It extends y if it needs to store more non-zero values than it
	 * currently has.
     * @param x
     * @param y
     * @return y
     */
    public SparseVector transMult(final SparseVector x, SparseVector y) throws Exception {
        if ( MTJRuntimeConfig.MTJDEBUG ) {
            checkTransMultAdd(x, y);
        }

        // We do not need to zero the y.data array here, because only first
        // non-zero elements will be assigned and counted in y.used
        // java.util.Arrays.fill(y.data, 0.0);
        int newYUsed = 0;
		// TODO: this loop is a bottleneck on rocksample_7-8: I need to skip some columns when I know that there are empty
		// TODO: columns; I could add another method that will take the list of non-zero columns!
        for (int i = 0; i < numColumns; ++i) {
			if ( colD[i].used == 0 ) continue;
            final double product = colD[i].dot(x);
            // zeros are implicit in the sparse vector, so we can skip them here
            if ( product != 0 ) {
                // We can build the y vector manually here; binary search of get()
                // is avoided.
                if ( ++newYUsed < y.data.length ) {
                    y.data[newYUsed - 1] = product;
                    y.index[newYUsed - 1] = i; // i is sorted so y.index will be fine
                } else {
                    // allocate twice as much, not less than 20, and no more than the overall size of the vector
                    int newLength = y.data.length != 0 ? Math.min( Math.max( y.data.length << 1, 20 ), y.size()) : 20;
                    // Copy existing data into new arrays
                    int[] newIndex = new int[newLength];
                    double[] newData = new double[newLength];
                    System.arraycopy(y.index, 0, newIndex, 0, y.index.length);
                    System.arraycopy(y.data, 0, newData, 0, y.data.length);
                    y.data = newData;
                    y.index = newIndex;
                    y.data[newYUsed - 1] = product;
                    y.index[newYUsed - 1] = i; // i is sorted so y.index will be fine
                }
            }
        }
        y.used = newYUsed;

        /*
        if ( MTJRuntimeConfig.MTJSLOWDEBUG ) {
            DenseVector xDense = new DenseVector(x);
            SparseVector y2 = new SparseVector(y);
            y2.zero();
            this.transMult(xDense, y2);
            for ( int i = 0; i < y2.size(); i++ ) {
                if ( y.get(i) != y2.get(i) ) {
                    System.err.println( xDense.toStr() );
                    System.err.println( this.toString() );
                    System.err.println( y.toStr() );
                    System.err.println( y2.toStr() );
                    throw new Exception("error here");
                }
            }
        }
        */

        return y;
    }

	/** the same as transMult but does rounding of probabilities */
	public SparseVector transMultProbs(final SparseVector x, SparseVector y, double epsilon) throws Exception {
		if ( MTJRuntimeConfig.MTJDEBUG ) {
			checkTransMultAdd(x, y);
		}

		// We do not need to zero the y.data array here, because only first
		// non-zero elements will be assigned and counted in y.used
		// java.util.Arrays.fill(y.data, 0.0);
		int newYUsed = 0;
		// TODO: this loop is a bottleneck on rocksample_7-8: I need to skip some columns when I know that there are empty
		// TODO: columns; I could add another method that will take the list of non-zero columns!
		for (int i = 0; i < numColumns; ++i) {
			if ( colD[i].used == 0 ) continue;
			final double product = colD[i].dot(x);
			// zeros are implicit in the sparse vector, so we can skip them here; here, we skip tiny values as well
			if ( product > epsilon ) {
				// We can build the y vector manually here; binary search of get()
				// is avoided.
				if ( ++newYUsed < y.data.length ) {
					y.data[newYUsed - 1] = product;
					y.index[newYUsed - 1] = i; // i is sorted so y.index will be fine
				} else {
					// allocate twice as much, not less than 20, and no more than the overall size of the vector
					int newLength = y.data.length != 0 ? Math.min( Math.max( y.data.length << 1, 20 ), y.size()) : 20;
					// Copy existing data into new arrays
					int[] newIndex = new int[newLength];
					double[] newData = new double[newLength];
					System.arraycopy(y.index, 0, newIndex, 0, y.index.length);
					System.arraycopy(y.data, 0, newData, 0, y.data.length);
					y.data = newData;
					y.index = newIndex;
					y.data[newYUsed - 1] = product;
					y.index[newYUsed - 1] = i; // i is sorted so y.index will be fine
				}
			}
		}
		y.used = newYUsed;

        /*
        if ( MTJRuntimeConfig.MTJSLOWDEBUG ) {
            DenseVector xDense = new DenseVector(x);
            SparseVector y2 = new SparseVector(y);
            y2.zero();
            this.transMult(xDense, y2);
            for ( int i = 0; i < y2.size(); i++ ) {
                if ( y.get(i) != y2.get(i) ) {
                    System.err.println( xDense.toStr() );
                    System.err.println( this.toString() );
                    System.err.println( y.toStr() );
                    System.err.println( y2.toStr() );
                    throw new Exception("error here");
                }
            }
        }
        */

		return y;
	}

	/** the simulator should call this method at the beginning and at the end; this methods clears indexes of non-empty
	  * columns which are required for sparse operations on columns */
	public void clearColsIndexes() {
		_usedCols = null;
		_numUsedCols = 0;
	}
	protected int[] _usedCols;
	protected int _numUsedCols;
	public SparseVector transMultProbsSparse(final SparseVector x, SparseVector y, double epsilon) throws Exception {
		if ( MTJRuntimeConfig.MTJDEBUG ) {
			checkTransMultAdd(x, y);
		}

		if ( _usedCols == null ) {
			_usedCols = new int[numColumns];
			_numUsedCols = 0;
			// We do not need to zero the y.data array here, because only first
			// non-zero elements will be assigned and counted in y.used
			// java.util.Arrays.fill(y.data, 0.0);
			int newYUsed = 0;
			// TODO: this loop is a bottleneck on rocksample_7-8: I need to skip some columns when I know that there are empty
			// TODO: columns; I could add another method that will take the list of non-zero columns!
			for (int i = 0; i < numColumns; ++i) {
				if ( colD[i].used == 0 ) continue;
				_usedCols[_numUsedCols++] = i;
				final double product = colD[i].dot(x);
				// zeros are implicit in the sparse vector, so we can skip them here; here, we skip tiny values as well
				if ( product > epsilon ) {
					// We can build the y vector manually here; binary search of get()
					// is avoided.
					if ( ++newYUsed < y.data.length ) {
						y.data[newYUsed - 1] = product;
						y.index[newYUsed - 1] = i; // i is sorted so y.index will be fine
					} else {
						// allocate twice as much, not less than 20, and no more than the overall size of the vector
						int newLength = y.data.length != 0 ? Math.min( Math.max( y.data.length << 1, 20 ), y.size()) : 20;
						// Copy existing data into new arrays
						int[] newIndex = new int[newLength];
						double[] newData = new double[newLength];
						System.arraycopy(y.index, 0, newIndex, 0, y.index.length);
						System.arraycopy(y.data, 0, newData, 0, y.data.length);
						y.data = newData;
						y.index = newIndex;
						y.data[newYUsed - 1] = product;
						y.index[newYUsed - 1] = i; // i is sorted so y.index will be fine
					}
				}
			}
			y.used = newYUsed;
		} else {
			// We do not need to zero the y.data array here, because only first
			// non-zero elements will be assigned and counted in y.used
			// java.util.Arrays.fill(y.data, 0.0);
			int newYUsed = 0;
			for (int k = 0; k < _numUsedCols; ++k) {
				int i = _usedCols[k];
				if ( colD[i].used == 0 ) continue;
				final double product = colD[i].dot(x);
				// zeros are implicit in the sparse vector, so we can skip them here; here, we skip tiny values as well
				if ( product > epsilon ) {
					// We can build the y vector manually here; binary search of get()
					// is avoided.
					if ( ++newYUsed < y.data.length ) {
						y.data[newYUsed - 1] = product;
						y.index[newYUsed - 1] = i; // i is sorted so y.index will be fine
					} else {
						// allocate twice as much, not less than 20, and no more than the overall size of the vector
						int newLength = y.data.length != 0 ? Math.min( Math.max( y.data.length << 1, 20 ), y.size()) : 20;
						// Copy existing data into new arrays
						int[] newIndex = new int[newLength];
						double[] newData = new double[newLength];
						System.arraycopy(y.index, 0, newIndex, 0, y.index.length);
						System.arraycopy(y.data, 0, newData, 0, y.data.length);
						y.data = newData;
						y.index = newIndex;
						y.data[newYUsed - 1] = product;
						y.index[newYUsed - 1] = i; // i is sorted so y.index will be fine
					}
				}
			}
			y.used = newYUsed;

		}
		return y;
	}

    @Override
    public void add(int row, int column, double value) {
        colD[column].add(row, value);
    }

    @Override
    public void set(int row, int column, double value) {
        colD[column].set(row, value);
    }

    @Override
    public double get(int row, int column) {
        return colD[column].get(row);
    }

    @Override
    public Iterator<MatrixEntry> iterator() {
        return new ColMatrixIterator();
    }

    @Override
    public FlexCompColMatrix copy() {
        return new FlexCompColMatrix(this);
    }

    @Override
    public FlexCompColMatrix zero() {
        for (int i = 0; i < numColumns; ++i)
            colD[i].zero();
        return this;
    }
    
    /**
     * Computes the largest element in the matrix.<b>This is not an absolute
     * value!<b>
     * @return
     */
    public double maxValue() {
        double max = Double.NEGATIVE_INFINITY;
        for (int i = 0; i < numColumns; ++i) {
            double tmp = colD[i].max();
            if ( tmp > max ) max = tmp;
        }
        return max;
    }

    /**
     * Computes the smallest element in the matrix.<b>This is not an absolute
     * value!<b>
     * @return min(this)
     */
    public double minValue() {
        double min = Double.POSITIVE_INFINITY;
        for (int i = 0; i < numColumns; ++i) {
            double tmp = colD[i].min();
            if ( tmp < min ) min = tmp;
        }
        return min;
    }
    
    /**
     * Tries to store the matrix as compactly as possible.
     * @see SparseVector#compact()
     */
    public void compact() {
        for (Vector v : colD)
            ((SparseVector) v).compact();
    }

    /**
     * @see SparseVector#hideExplicitZeros() 
     */
    public void hideExplicitZeros() {
        for (Vector v : colD)
            ((SparseVector) v).hideExplicitZeros();
    }

    /**
     * @see SparseVector#hideSmallValues(double)
     * @param epsilon
     */
    public void hideSmallValues(double epsilon) {
        for (Vector v : colD)
            ((SparseVector) v).hideSmallValues(epsilon);
    }
    
    /**
     * @see SparseVector#explicitZerosExist() 
     */
    public boolean explicitZerosExist() {
        for (Vector v : colD) {
            if ( ((SparseVector) v).explicitZerosExist() ) {
                return true;
            }
        }
        return false;
    }
    
    /**
     * Iterator over a matrix stored vectorwise by columns
     */
    private class ColMatrixIterator implements Iterator<MatrixEntry>, Serializable {

        /**
         * Iterates over each column vector
         */
        private SuperIterator<SparseVector, VectorEntry> iterator = new SuperIterator<SparseVector, VectorEntry>(
                Arrays.asList(colD));

        /**
         * Entry returned
         */
        private ColMatrixEntry entry = new ColMatrixEntry();

        public boolean hasNext() {
            return iterator.hasNext();
        }

        public MatrixEntry next() {
            SuperIteratorEntry<VectorEntry> se = iterator.next();
            entry.update(se.index(), se.get());
            return entry;
        }

        public void remove() {
            iterator.remove();
        }

    }

    /**
     * Entry of a matrix stored vectorwise by columns
     */
    private static class ColMatrixEntry implements MatrixEntry, Serializable {

        private int column;

        private VectorEntry entry;

        public void update(int column, VectorEntry entry) {
            this.column = column;
            this.entry = entry;
        }

        public int row() {
            return entry.index();
        }

        public int column() {
            return column;
        }

        public double get() {
            return entry.get();
        }

        public void set(double value) {
            entry.set(value);
        }

    }

	// -----
	// Methods related to POMDPs

	/**
	 * Normally, observations can be sampled using obsVector = b * Taso. This method does not compute obsVector, instead
	 * it returns the observation directly.
	 * @see #transMult(SparseVector, SparseVector)
	 * @param x
	 * @param rand
	 * @return
	 * @throws Exception
	 */
	public int transMultGetSample(final SparseVector x, double rand) throws Exception {
		if ( MTJRuntimeConfig.MTJDEBUG ) {
			if ( x.size() != this.numRows() ) {
				throw new Exception("wrong vector in this multiplication");
			}
		}

		double sum = 0.0;
		int lastNonZero = -1;
		/*
		// Print probabilities of all observations
		for (int i = 0; i < numColumns; ++i) {
			if ( colD[i].used == 0 ) continue;
			final double product = colD[i].dot(x);
			System.out.print("prob(obs=" + i + ")=" + product + ",");
		}
		System.out.println();
		*/
		// TODO: this loop is a bottleneck on rocksample_7-8: I need to skip some columns when I know that there are empty
		// TODO: columns; I could add another method that will take the list of non-zero columns!
		for (int i = 0; i < numColumns; ++i) {
			if ( colD[i].used == 0 ) continue;
			final double product = colD[i].dot(x);
			// we skip zeros because we want to skip observations that have zero probability
			if ( product != 0 ) {
				sum += product;
				lastNonZero = i;
				if ( sum >= rand ) {
					if ( MTJRuntimeConfig.MTJSLOWDEBUG ) {
						for ( int j = i + 1; j < numColumns; j++ ) {
							double p = colD[j].dot(x);
							sum += p;
						}
						if ( Math.abs( sum - 1.0) > 1e-3 ) {
							throw new Exception("the probability distribution does not sum to 1.0 " + sum);
						}
					}
					return i;
				}
			}
		}

		if ( Math.abs( sum - 1.0) > 1e-3 ) {
			throw new Exception("the probability distribution does not sum to 1.0 " + sum);
		}

		// return last non-zero element
		return lastNonZero;
	}
}
