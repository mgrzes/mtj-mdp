package no.uib.cipr.matrix.sparse;

/**
 * Created with IntelliJ IDEA.
 * User: Marek Grzes
 * Date: 07/05/13 3:42 PM
 */

/** A sparse vector class that performs approximate comparison on SparseVectors */
public class SparseVectorApprox extends SparseVector {
	public static double _APPROXTHRESHOLD = 1E-8;

	public SparseVectorApprox(int size, int nz) {
		super(size, nz);
	}

	@Override
	public boolean equals(Object other) {
		return super.equals(other, _APPROXTHRESHOLD);
	}
}
