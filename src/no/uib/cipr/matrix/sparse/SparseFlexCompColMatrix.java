package no.uib.cipr.matrix.sparse;

import no.uib.cipr.matrix.MTJRuntimeConfig;

/**
 * Created with IntelliJ IDEA.
 * User: Marek Grzes
 * Date: 27/05/13 4:34 PM
 */
public class SparseFlexCompColMatrix extends FlexCompColMatrix {

	/**
	 * Column index that stores non-zero columns.
	 */
	int[] _colIndex;

	/** The number of used columns. colIndex may allocate more memory for speed. */
	int _colUsed;

	public SparseFlexCompColMatrix(int numRows, int numColumns) {
		super(numRows, numColumns);
	}

	public SparseFlexCompColMatrix(int numRows, int numColumns, boolean initColumns) {
		super(numRows, numColumns, initColumns);
	}

	public void computeColumnIndex() {
		if ( _colIndex == null ) _colIndex = new int[numColumns];
		_colUsed = 0;
		for (int i = 0; i < numColumns; ++i) {
			if ( colD[i].used > 0 ) {
				_colIndex[_colUsed++] = i;
			}
		}
	}

	public SparseVector transMult(final SparseVector x, SparseVector y) throws Exception {

		if ( _colIndex == null ) {
			return super.transMult(x, y);
		}

		if ( MTJRuntimeConfig.MTJDEBUG ) {
			checkTransMultAdd(x, y);
		}

		if ( MTJRuntimeConfig.MTJSLOWDEBUG ) {
			int c = 0;
			int num = 0;
			for (int i = 0; i < numColumns; ++i) {
				if ( colD[i].used != 0 ) {
					num++;
					if ( _colIndex[c++] != i ) throw new Exception("computeColumnIndex() was probably not executed before transMult()");
				}
			}
			if ( num != _colUsed ) throw new Exception("computeColumnIndex() was probably not executed before transMult()");
		}

		// We do not need to zero the y.data array here, because only first
		// non-zero elements will be assigned and counted in y.used
		// java.util.Arrays.fill(y.data, 0.0);
		int newYUsed = 0;
		for (int c = 0; c < _colUsed; ++c) {
			int i = _colIndex[c];
			if ( colD[i].used == 0 ) {
				continue;
			}
			final double product = colD[i].dot(x);
			// zeros are implicit in the sparse vector, so we can skip them here
			if ( product != 0 ) {
				// We can build the y vector manually here; binary search of get()
				// is avoided.
				if ( ++newYUsed < y.data.length ) {
					y.data[newYUsed - 1] = product;
					y.index[newYUsed - 1] = i; // i is sorted so y.index will be fine
				} else {
					// allocate twice as much, not less than 20, and no more than the overall size of the vector
					int newLength = y.data.length != 0 ? Math.min( Math.max( y.data.length << 1, 20 ), y.size()) : 20;
					// Copy existing data into new arrays
					int[] newIndex = new int[newLength];
					double[] newData = new double[newLength];
					System.arraycopy(y.index, 0, newIndex, 0, y.index.length);
					System.arraycopy(y.data, 0, newData, 0, y.data.length);
					y.data = newData;
					y.index = newIndex;
					y.data[newYUsed - 1] = product;
					y.index[newYUsed - 1] = i; // i is sorted so y.index will be fine
				}
			}
		}
		y.used = newYUsed;

        /*
        if ( MTJRuntimeConfig.MTJSLOWDEBUG ) {
            DenseVector xDense = new DenseVector(x);
            SparseVector y2 = new SparseVector(y);
            y2.zero();
            this.transMult(xDense, y2);
            for ( int i = 0; i < y2.size(); i++ ) {
                if ( y.get(i) != y2.get(i) ) {
                    System.err.println( xDense.toStr() );
                    System.err.println( this.toString() );
                    System.err.println( y.toStr() );
                    System.err.println( y2.toStr() );
                    throw new Exception("error here");
                }
            }
        }
        */

		return y;
	}
}
