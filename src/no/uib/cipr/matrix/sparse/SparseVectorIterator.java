package no.uib.cipr.matrix.sparse;

/**
 * Created with IntelliJ IDEA.
 * User: Marek Grzes
 * Date: 24/02/14 11:10 AM
 */

import java.util.Iterator;

/**
 * Vector cannot change externally. hasNext() has to be executed before every next().
 */
public class SparseVectorIterator implements Iterator<PairIntDouble> {

	protected SparseVector _vector;
	protected int _position;
	protected PairIntDouble _result;

	public SparseVectorIterator(SparseVector vector) {
		_vector = vector;
		_position = 0;
		_result = new PairIntDouble();
	}

	public boolean hasNext() {
		// skip all zero entries
		while ( _position < _vector.used && _vector.data[_position] == 0 ) {
			_position++;
		}
		return _position < _vector.used;
	}

	public PairIntDouble next() {
		if ( _position >= _vector.used ) return null;

		_result.first = _vector.index[_position];
		_result.second = _vector.data[_position];
		_position++;

		return _result;
	}

	public void remove() {
		// TODO: implement removing from the vector
	}

	/**
	 * reset iterator to the beginning; the vector may be changed externally before this method is called;
	 */
	public void reset() {
		_position = 0;
	}

	public void reset(SparseVector vector) {
		_vector = vector;
		_position = 0;
	}
}
