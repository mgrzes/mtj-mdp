package no.uib.cipr.matrix.sparse;

import junit.framework.TestCase;

/**
 * Created with IntelliJ IDEA.
 * User: Marek Grzes
 * Date: 25/03/13 11:28 AM
 */
public class FlexCompRowMatrix2Test extends TestCase {
	public void testcopyAsColMatrix() throws Exception {
		FlexCompRowMatrix rMat = new FlexCompRowMatrix(5,5);
		rMat.set(0, 0, 100);
		rMat.set(3, 3, 133);
		rMat.set(4, 2, 142);
		System.out.println(rMat.toString());
		FlexCompColMatrix cMat = rMat.copyAsColMatrix();
		System.out.println(cMat.toString());
		assertTrue(rMat.get(3,3) == cMat.get(3,3) && rMat.get(4,2) == cMat.get(4,2));
	}
}
