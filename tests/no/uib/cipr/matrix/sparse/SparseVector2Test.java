package no.uib.cipr.matrix.sparse;

import junit.framework.TestCase;
import no.uib.cipr.matrix.DenseVector;
import no.uib.cipr.matrix.VectorEntry;

import java.util.Iterator;

/**
 * Created with IntelliJ IDEA.
 * User: Marek Grzes
 * Date: 22/03/13 10:43 AM
 */
public class SparseVector2Test extends TestCase {

	public void testEntropy() throws Exception {
		SparseVector v = new SparseVector(10);
		v.set(0,0.5);
		System.out.println(v.toStr() + ",\t\tentropy is " + v.entropy());
		v.set(1,0.25);
		System.out.println(v.toStr() + ",\t\tentropy is " + v.entropy());
		v.set(2,0.2);
		System.out.println(v.toStr() + ",\t\tentropy is " + v.entropy());
		v.set(3,0.05);
		System.out.println(v.toStr() + ",\t\tentropy is " + v.entropy());
		v.set(5,1000.0);
		System.out.println(v.toStr() + ",\t\tentropy is " + v.entropy());
	}

	public void testappendFrom() throws Exception {
		SparseVector v = new SparseVector(20);
		v.set(0, 10);

		double[] arr = new double[10];

		arr[5] = 105;
		arr[8] = 108;

		v.appendFrom(2, arr);

		System.out.println(v.toStr());
	}

	public void minusScaledCompatible2NonNeg() throws Exception {
		SparseVector beliefI = new SparseVector(20);
		beliefI.set(0, 0.3);
		beliefI.set(4, 0.3);
		beliefI.set(19,0.4);
		SparseVector belief = beliefI.copy();

		belief.minusScaledCompatible2NonNeg(1.0, beliefI);

		assertTrue(belief.sum() == 0.0);
	}

	public void testCompatibleWithBug1() throws Exception {
		SparseVector belief = new SparseVector(20);
		belief.set(0, 0.85);
		belief.set(1,0.15);
		SparseVector beliefI = new SparseVector(20);
		beliefI.set(0, 0.3);
		beliefI.set(4,0.3);
		beliefI.set(19,0.4);
		try {
			assertTrue(!beliefI.compatibleWith(belief));
		}
		catch (Exception e) {
			e.printStackTrace();
			System.err.println(e.toString());
			assertTrue(false);
		}
	}

	public void test_getCopyAsDenseArray() throws Exception {
		SparseVector v = new SparseVector(5);
		v.set(0, 0);
		v.set(1, 1);
		v.set(3, 3);
		v.set(4, 4);
		SparseVectorMask mask = new SparseVectorMask(3);
		mask._index = new int[]{ 0, 1, 4};
		mask._used = 3;

		double[] dense = v.getCopyAsDenseArray(mask);
		for ( double d : dense ) {
			System.out.print(d + " ");
		}
		System.out.println();
		assertTrue("expected value 4", dense[2] == 4.0 );
	}

	public void test_addDiffScale() throws Exception {

		SparseVector v = new SparseVector(5);
		v.set(0, 0);
		v.set(1, 1);
		v.set(3, 3);
		v.set(4, 4);
		SparseVectorMask mask = new SparseVectorMask(3);
		mask._index = new int[]{ 0, 1, 4};
		mask._used = 3;

		SparseVector v2 = new SparseVector(5);
		SparseVector res = new SparseVector(5);
		res.addDiffScale(v, 0.5, v2, mask);

		System.out.println(res.toStr());
		assertTrue("expected value 4.0 but found " + res.get(4), res.get(4) == 4.0 );

		v2 = new SparseVector(5);
		v2.set(3, 1);
		v2.set(4, 2);
		res = new SparseVector(5);
		res.addDiffScale(v, 0.5, v2, mask);

		System.out.println(res.toStr());
		assertTrue("expected value 3.0 but found " + res.get(4), res.get(4) == 3.0 );

		// -----

		res = new SparseVector(5);
		res.addDiffScale(v, 0.5, v2);

		System.out.println(res.toStr());
		assertTrue("expected value 3.0 but found " + res.get(4), res.get(4) == 3.0 );

		v2 = new SparseVector(5);
		res = new SparseVector(5);
		res.addDiffScale(v, 0.5, v2);
		System.out.println(res.toStr());
		assertTrue("expected value 4.0 but found " + res.get(4), res.get(4) == 4.0 );

	}

	public void test_minusScale() throws Exception {

		SparseVector v = new SparseVector(5);
		v.set(0, 0);
		v.set(1, 1);
		v.set(3, 3);
		v.set(4, 4);
		SparseVectorMask mask = new SparseVectorMask(3);
		mask._index = new int[]{ 0, 1, 4};
		mask._used = 3;

		SparseVector v2 = new SparseVector(5);
		v2.set(0,10);
		v2.set(1,10);
		v2.set(2,10);
		v2.set(3,10);
		v2.set(4,10);
		v2.minusScale(0.5, v, mask);

		System.out.println(v2.toStr());
		assertTrue("expected value 8", v2.get(4) == 8.0 );
	}

	public void test_pushNextStateTo() throws Exception {

		SparseVector v = new SparseVector(15);

		v.pushNextStateTo(v, 0, 0.5);
		v.pushNextStateTo(v, 5, 7);
		v.pushNextStateTo(v, 7, 5);
		v.pushNextStateTo(v, 13, 4);

		System.out.println(v.toStr());
		assertTrue("expected value 4", v.get(13) == 4 );

	}

	public void test_compact() {
		double[] tfVector = {0.0,0.5,0.0,0.4,0.0};
		DenseVector dense= new DenseVector(tfVector, false);

		// vectroTF is made compact during creation so the test would not show anything
		SparseVector vectorTF =  new SparseVector(dense);

		// in order to test compact() we need to create the sparse vector manually and enforce dense representation
		int[] tfIndex = {0, 1, 2, 3, 4 };
		vectorTF =  new SparseVector(5, tfIndex, tfVector);
		vectorTF.compact();

		assertTrue(vectorTF.getUsed() == 2);  // vectorTF.getUsed() returns 5

		for (Iterator<VectorEntry> it = vectorTF.iterator();it.hasNext();) {
			VectorEntry ve= it.next();
			int index = ve.index();
			double value = ve.get();
			assertTrue(tfVector[index]== value);
		}

		vectorTF =  new SparseVector(5, tfIndex, tfVector);
		vectorTF.hideExplicitZeros();

		assertTrue(vectorTF.getUsed() == 2);  // vectorTF.getUsed() returns 5

		for (Iterator<VectorEntry> it = vectorTF.iterator();it.hasNext();) {
			VectorEntry ve= it.next();
			int index = ve.index();
			double value = ve.get();
			assertTrue(tfVector[index]== value);
		}

	}
}
